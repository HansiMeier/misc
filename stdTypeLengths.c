#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#define STRING_FORMAT "%-16s"

#define outputTypeLength(type) \
   printf(STRING_FORMAT "\t%3zu\n", #type, sizeof(type)*8)


int main() {
   printf(STRING_FORMAT "\tlength in bits\n", "type");
   puts("-------------------------------------------");
    outputTypeLength(char);
    outputTypeLength(short);
    outputTypeLength(int);
    outputTypeLength(long);
    outputTypeLength(long long);
    outputTypeLength(size_t);
    outputTypeLength(ptrdiff_t);
    outputTypeLength(off_t);
    outputTypeLength(float);
    outputTypeLength(double);
    outputTypeLength(long double);
    outputTypeLength(void*);
    outputTypeLength(int(*)(int));
   return 0;
}
