/**************************************************************************************************
 *                                         KNAPSACK FPTAS                                         *
 **************************************************************************************************/
#ifndef KNAPSACK_HPP
#define KNAPSACK_HPP

#include <tuple>
#include <vector>
template <typename Num>
std::tuple<Num, std::vector<int>> knapsack(
      const std::vector<Num>& weights,
      std::vector<Num> values,
      Num weightLimit,
      double errorMargin = 0.0);
#include "knapsack.cpp"
#endif

