#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <png.h>



/**************************************************************************************************
 *                                          PNG I/O IMPL                                          *
 **************************************************************************************************/

#define guardReadOrDie(cond, error, fp, png_ptrp, info_ptrp, rowPointers, currentRowp) \
   if (!(cond)) { \
      if (fp) {\
         fclose(fp); \
         fp = NULL; \
      } \
      png_destroy_read_struct( \
            png_ptrp, \
            info_ptrp, \
            NULL \
         ); \
      if (png_ptrp) { \
         *png_ptrp = NULL; \
      } \
      if (info_ptrp) { \
         *info_ptrp = NULL; \
      } \
      if (rowPointers) { \
         free(rowPointers); \
         rowPointers = NULL; \
      } \
      *currentRowp = 0; \
      return error; \
   }

// Cleanup all the relevant structures if the cond fails
#define guardReadOrDieCleanupAll(cond, error) \
   guardReadOrDie(cond, error, fp, (png_ptr? &png_ptr : NULL), (info_ptr? &info_ptr : NULL), rowPointers, &currentRow);

// read data from a PNGFile either rowwise or whole
// and let bufferp point to its location
// and sizep to its size
encodeInPNG_error_t readDataAsPNG(
      const char* filename,
      uintmax_t* sizep,
      char** bufferp,
      bool rowwise

) {

   static uintmax_t currentRow = 0;
   static uintmax_t height = 0;
   static png_structp png_ptr = NULL;
   static png_infop info_ptr = NULL;
   static FILE* fp = NULL;
   uintmax_t rowSize = 0;
   png_bytepp rowPointers = NULL;


   // enable special cleanup call if the first three arguments are all NULL
   guardReadOrDieCleanupAll(bufferp || sizep || filename, CLEANED_UP);

   if (! currentRow) {
      // INITIALIZATION SECTION
      // The user needs to know the data size
      guardReadOrDieCleanupAll(sizep, ERR_ALLOC);

      fp = fopen(filename, "rb");

      if (!fp) {
         return ERR_OPEN;
      }

      // check that it is a PNG
      uint64_t header;
      fread(&header, 1, sizeof(header) /* should be 8!*/, fp);
      // check for a PNG signature
      // png_sig_cmp uses errorlevel semantics
      guardReadOrDieCleanupAll(
            !png_sig_cmp((png_bytep) &header, 0, sizeof(header)),
            ERR_SIGNATURE
         );

      // Initialise opaque png_struct and png_info
      // The NULLs mean use default libpng error handlers
      png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

      guardReadOrDieCleanupAll(png_ptr, ERR_PNGSTRUCT);
      info_ptr = png_create_info_struct(png_ptr);

      guardReadOrDieCleanupAll(info_ptr, ERR_PNGSTRUCT);

      // make a longjmp target for libpng to jump back to
      // if init_io failed
      guardReadOrDieCleanupAll(!setjmp(png_jmpbuf(png_ptr)), ERR_IO);

      png_init_io(png_ptr, fp);
      png_set_sig_bytes(png_ptr, sizeof(header)); // tell libpng that we already read the header
      png_read_info(png_ptr, info_ptr);

      // Check that the image is not interlaced
      guardReadOrDieCleanupAll(
            png_get_interlace_type(png_ptr, info_ptr) == PNG_INTERLACE_NONE,
            ERR_INTERLACED
         );



      rowSize = png_get_rowbytes(png_ptr, info_ptr);

      height = png_get_image_height(png_ptr, info_ptr);
      png_read_update_info(png_ptr, info_ptr);
      *sizep = rowSize;
      if (! rowwise) {
         // If it is requested to slurp the whole file, write
         // the whole size, not just the size of a row
         *sizep *= height;
      }


      // Allocate the buffer
      // either just a single row
      // or the full image
      *bufferp = malloc(*sizep);
      guardReadOrDieCleanupAll(*bufferp, ERR_ALLOC);
   }

   // READ SECTION
   if (rowwise) {
      // read file rowwise
      if (currentRow++ != height) {
         png_read_row(png_ptr, (png_bytep) *bufferp, NULL);
      }
   } else {
      rowPointers = malloc(height * sizeof(png_bytep));
      guardReadOrDieCleanupAll(rowPointers, ERR_ALLOC);

      // let the rows point to the buffer
      for (uintmax_t h = 0, offset = 0; h != height; ++h, offset += rowSize) {
        rowPointers[h] = ((png_bytep) *bufferp) + offset;
      }

      // slurp
      png_read_image(png_ptr, rowPointers);
   }

   if (!rowwise) {
      // We've just slurped the whole image
      guardReadOrDieCleanupAll(false, SUCCESS);
   }

   if (currentRow == height+1) {
      // We've read the whole image rowwise
      currentRow = 0;
      guardReadOrDieCleanupAll(false, FINISHED);
   }

   // We've just read a row
   return SUCCESS;

}

#undef guardReadOrDie
#undef guardReadOrDieCleanupAll

// Given pixels, how can I arrange them in a rectangle such that it
// as square as possible? -> writes the width and height of such a rectangle into the pointers given
bool calcWidthHeight(
      uintmax_t pixels,
      png_uint_32p const widthp,
      png_uint_32p const heightp
) {

   *widthp = 1;
   *heightp = 1;

   // the root for the trial division
   const uintmax_t root = sqrt(pixels) + 0.5;
   
   size_t primeFactorsSize = 40 * sizeof(png_uint_32);
   png_uint_32p primeFactors = malloc(primeFactorsSize);
   png_uint_32p currentFactorp = primeFactors;
   png_uint_32p primeFactorsEnd = primeFactors + primeFactorsSize;


   size_t newSize;
   png_uint_32p newPrimeFactors;

   #define addToPrimeFactors(maybeDivisor) \
      if (currentFactorp == primeFactorsEnd) { \
         newSize = 2 * primeFactorsSize; \
         newPrimeFactors = realloc(primeFactors, newSize); \
         if (! newPrimeFactors) { \
            free(primeFactors); \
            return false; \
         } \
         primeFactors = newPrimeFactors; \
         currentFactorp = newPrimeFactors; \
         primeFactorsEnd = newPrimeFactors + newSize; \
         primeFactorsSize = newSize; \
      } \
      *currentFactorp++ = maybeDivisor; \
      pixels /= maybeDivisor;

   // Sort out twos such that we can use a step of 2 instead of 1 afterwards
   while (!(pixels % 2)) {
      addToPrimeFactors(2);
   }

   // get all prime factors
   for (
        uintmax_t maybeDivisor = 3;
        maybeDivisor <= root && pixels != 1;
        maybeDivisor += 2
   ) {
      while (!(pixels % maybeDivisor)) {
         addToPrimeFactors(maybeDivisor);
      }
   }
   #undef addToPrimeFactors

   // end here if it's prime
   if (pixels != 1) {
      *heightp = pixels;
      free(primeFactors);
      return true;
   }

   // distribute prime factors
   png_uint_32p writep = heightp;
   png_uint_32p swapp = widthp;
   png_uint_32p tmpp;
   for (png_uint_32p factorp = currentFactorp - 1; factorp >= primeFactors; --factorp) {
      *writep *= *factorp;
      if (*writep > *swapp) {
         // Swap to distribute evenly
         tmpp = writep;
         writep = swapp;
         swapp = tmpp;
      }
   }

   // Make sure it's in portrait format
   if (*heightp < *widthp) {
      png_uint_32 tmp = *heightp;
      *heightp = *widthp;
      *widthp = tmp;
   }

   #undef giveFactorAndSwap
   free(primeFactors);
   return true;
}

void choosePNGSettings(
      uintmax_t* pixelsp,
      png_bytep colourTypep,
      png_bytep bitDepthp
) {

   uintmax_t pixels = *pixelsp;

   if (!(pixels % 4)) {
      *colourTypep = PNG_COLOR_TYPE_RGB_ALPHA;
      pixels /= 4;
   } else if (!(pixels % 3)) {
      *colourTypep = PNG_COLOR_TYPE_RGB;
      pixels /= 3;
   } else if (!(pixels % 2)) {
      *colourTypep = PNG_COLOR_TYPE_GRAY_ALPHA;
      pixels /= 2;
   } else {
      *colourTypep = PNG_COLOR_TYPE_GRAY;
   }


   if (!(pixels % 2)) {
      *bitDepthp = 16;
      pixels /= 2;
   } else {
      *bitDepthp = 8;
   }

   *pixelsp = pixels;

   return;
}

void setCompressionLevel(png_structp png_ptr, const uintmax_t size) {
   const uintmax_t M = 1024*1024;
   intmax_t level = 6 - size / (50*M);
   if (level < 0) {
      level = 0;
   }

   png_set_compression_level(png_ptr, level);
   return;
}
   
#define guardWriteOrDie(cond, error, fp, png_ptrp, info_ptrp, rowPointers) \
   if (!(cond)) { \
      if (fp) {\
         fclose(fp); \
         fp = NULL; \
      } \
      png_destroy_write_struct( \
            png_ptrp, \
            info_ptrp \
         ); \
      if (png_ptrp) { \
         *png_ptrp = NULL; \
      } \
      if (info_ptrp) { \
         *info_ptrp = NULL; \
      } \
      if (rowPointers) { \
         free(rowPointers); \
         rowPointers = NULL; \
      } \
      return error; \
   }
#define guardWriteOrDieCleanupAll(cond, error) \
   guardWriteOrDie(cond, error, fp, (png_ptr? &png_ptr : NULL), (info_ptr? &info_ptr : NULL), rowPointers);
   
   
encodeInPNG_error_t writePNG(
      const char* filename,
      const size_t filenameLength,
      const uintmax_t size,
      const char* buffer
) {
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   png_bytepp rowPointers = NULL;



   FILE* fp = fopen(filename, "wb");
   guardWriteOrDieCleanupAll(fp, ERR_OPEN);
   guardWriteOrDieCleanupAll(size, ERR_IO);

   png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
   guardWriteOrDieCleanupAll(png_ptr, ERR_PNGSTRUCT);

   info_ptr = png_create_info_struct(png_ptr);
   guardWriteOrDieCleanupAll(info_ptr, ERR_PNGSTRUCT);

   // make a longjmp target for libpng to jump back to
   // if init_io failed
   guardWriteOrDieCleanupAll(!setjmp(png_jmpbuf(png_ptr)), ERR_IO);

   png_init_io(png_ptr, fp);

   uintmax_t pixels = size;
   png_byte colourType, bitDepth;
   png_uint_32 width, height;

   choosePNGSettings(&pixels, &colourType, &bitDepth);
   setCompressionLevel(png_ptr, size);
   guardWriteOrDieCleanupAll(calcWidthHeight(pixels, &width, &height), ERR_ALLOC);

   png_set_IHDR(
         png_ptr, info_ptr, width, height,
         bitDepth, colourType, PNG_INTERLACE_NONE,
         PNG_COMPRESSION_TYPE_DEFAULT,
         PNG_FILTER_TYPE_DEFAULT
      );

   png_write_info(png_ptr, info_ptr);

   rowPointers = malloc(height * sizeof(png_bytep));
   guardWriteOrDieCleanupAll(rowPointers, ERR_ALLOC);


   // let the rows point to the buffer
   uintmax_t rowSize = size / height;
   for (uintmax_t h = 0, offset = 0; h != height; ++h, offset += rowSize) {
     rowPointers[h] = ((png_bytep) buffer) + offset;
   }

   png_write_image(png_ptr, rowPointers);

   png_write_end(png_ptr, info_ptr);
   guardWriteOrDieCleanupAll(false, SUCCESS);
   return SUCCESS;
}
#undef guardWriteOrDie
#undef guardWriteOrDieCleanupAll

