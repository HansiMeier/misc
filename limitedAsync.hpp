#pragma once
#include <vector>
#include <thread>
#include <cassert>

template <typename Function, typename BeginIterator, typename EndIterator>
decltype(auto) // A std::vector of returnvalues of the function f
(
    FirstIterator begin,
    EndIterator end,
    Function f,
    const unsigned maxNumberOfThreads
) {
    assert(maxNumberOfThreads > 0);



