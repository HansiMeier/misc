#include <iostream>
#include <mutex>
#include <cassert>
#include <memory>

const int BEGIN_SENTINEL(-1);
const int END_SENTINEL(0);

struct ListNode {
    int number;
    std::unique_ptr<ListNode> next;
    std::mutex mutex;
};

void
lockNode(
    ListNode* node
) {
    assert(node != nullptr);
    #ifndef NDEBUG
    std::cout << "Locking node with content " << node->number << "\n";
    std::cout.flush();
    #endif
    return node->mutex.lock();
}

void
unlockNode(
    ListNode* node
) {
    assert(node != nullptr);
    #ifndef NDEBUG
    std::cout << "Unlocking node with content " << node->number << "\n";
    std::cout.flush();
    #endif
    return node->mutex.unlock();
}


ListNode*
walkToJustBeforeNumber(
    int number,
    ListNode* head
) {
    #ifndef NDEBUG
    std::cout << "Walking before node " << number << "\n";
    #endif
    assert(head && head->next);
    lockNode(head);
    ListNode* previous(head);

    ListNode* current(head->next.get());
    lockNode(current);
    ListNode* next(current->next.get());

    while (next != nullptr && current->number != number) {
        lockNode(next);
        unlockNode(previous);
        previous = current;
        current = next;
        next = next->next.get();
    }


    if (current->number == number) {
        #ifndef NDEBUG
        std::cout << "Found number " << number << " after number " << previous->number << "\n";
        #endif
        return previous;
    } else {
        #ifndef NDEBUG
        std::cout << "Didn't find number " << number << "\n";
        #endif
        previous->mutex.unlock();
        current->mutex.unlock();
        return nullptr;
    }
}

std::unique_ptr<ListNode>
makeEmptyList(
) {
    std::unique_ptr<ListNode> head(new ListNode);
    std::unique_ptr<ListNode> tail(new ListNode);
    tail->number = END_SENTINEL;
    tail->next = nullptr;
    head->number = BEGIN_SENTINEL;
    head->next = std::move(tail);

    return std::move(head);
}


void
remove(
    int number,
    ListNode* head
) {
    #ifndef NDEBUG
    std::cout << "Removing node with content " << number << "\n";
    std::cout.flush();
    #endif
    assert(number != BEGIN_SENTINEL && number != END_SENTINEL);
    ListNode* beforeToRemove(walkToJustBeforeNumber(number, head));
    if (beforeToRemove == nullptr) {
        return;
    }

    ListNode* toRemove(beforeToRemove->next.get());

    assert(beforeToRemove->next->number == number && toRemove != nullptr);

    beforeToRemove->next = std::move(toRemove->next);
    unlockNode(beforeToRemove);
    delete toRemove;
}

void
insertJustBefore(
    int beforeThis,
    int numberToInsert,
    ListNode* head
) {
    #ifndef NDEBUG
    std::cout << "Inserting node with content " << numberToInsert << " before node with content " << beforeThis << "\n";
    std::cout.flush();
    #endif
    ListNode* nodeBefore(walkToJustBeforeNumber(beforeThis, head));
    assert(nodeBefore != nullptr);
    std::unique_ptr<ListNode> nodeAfter(std::move(nodeBefore->next));
    assert(nodeAfter != nullptr && nodeBefore != nullptr);
    std::unique_ptr<ListNode> newNode(new ListNode);

    newNode->number = numberToInsert;
    newNode->next = std::move(nodeAfter);
    nodeBefore->next = std::move(newNode);

    assert(newNode->next != nullptr);
    unlockNode(nodeBefore);

    return;
}

void
printList(
    ListNode* head
) {
    for (; head != nullptr; head = head->next.get()) {

        std::cout << head->number << " ";
    }
    std::cout << "\n";
}


int main(
) {

    std::unique_ptr<ListNode> list(makeEmptyList());
    ListNode* head(list.get());

    

    return 0;
}

