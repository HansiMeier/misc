#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#ifdef __cplusplus
extern "C" {
#endif


#if defined(__cplusplus) && __cplusplus >= 201103L
    [[noreturn]]
#elif defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
    _Noreturn
#elif defined(__GNUC__)
    __attribute__((noreturn))
#elif defined(_MSC_VER)
    __declspec(noreturn)
#else
    #warning "Didn't find suitable implementation of macro DOESNT_RETURN; defined it to be empty"
#endif
static inline
void
fatalError(
    const char* formatString,
    ...
) {
    va_list args;
    va_start(args, formatString);


    vfprintf(stderr, formatString, args);
    va_end(args);
    fputs("", stderr);

    abort();
}

#ifdef __cplusplus
}
#endif
#undef FATAL_ERROR_DOESNT_RETURN
