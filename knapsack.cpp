#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#define NDEBUG
#include <cassert>
#include <limits>
#include <tuple>

#if __cplusplus < 201103L
   #warning "This is a C++11 source! Are you sure you are compiling this with -std=c++11?"
#endif


// New sort of typedefs
// not all are needed now
template <typename Num> using Vec = std::vector<Num>;
template <typename Num> using It = typename Vec<Num>::iterator;
template <typename Num> using Cit = typename Vec<Num>::const_iterator;
template <typename Num> using Tbl = std::vector<Vec<Num>>;
template <typename Num> using Tit = typename Tbl<Num>::iterator;
template <typename Num> using Tcit = typename Tbl<Num>::const_iterator;

// The polynomial approximation scheme to the knapsack problem using the
// minimum weight variant.
// Num has to be an integral type!
template <typename Num>
std::tuple<Num, Vec<int>> knapsack(const Vec<Num>& weights, Vec<Num> values, Num weightLimit, double errorMargin) {

   // Make sure Num is an integral type
   assert(std::numeric_limits<Num>::is_integer);

   // check preconditions
   assert(weights.size() == values.size());
   assert(weights.size() != 0);
   assert(errorMargin >= 0 && errorMargin < 1);
   assert(weightLimit >= 0);


   // roundoff the values to have less work
   if (errorMargin != 0) {
      double divisor (errorMargin * double (*std::max_element(values.begin(), values.end())));
      if (divisor != 0.0) {
         double size (values.size());
         for (It<Num> it (values.begin()); it != values.end(); ++it) {
            *it = static_cast<Num> ((*it) * size / divisor);
         }
      }
   }

   // make the dynamic programming table of size (Number of Objects) x (Sum of values)
   const size_t numOfCols (std::accumulate(values.cbegin(), values.cend(), 0));
   const size_t numOfRows (values.size());
   Tbl<Num> table (numOfRows, Vec<Num> (numOfCols));

   // edge cases

   // take the first object as long as possible
   for (size_t j = 0; j < values[0]; ++j) {
      table[0][j] = weights[0];
   }


   // Get a value as near as possible to Inf
   // to use as impossible value
   // Has to be bigger than any allowed value
   // leave a little room as not to overflow
   const Num inf (std::numeric_limits<Num>::max() - std::accumulate(weights.cbegin(), weights.cend(), 0));

   // fill out the impossible cases with inf
   for (size_t j = values[0]; j < numOfCols; ++j) {
      table[0][j] = inf;
   }


   // regular cases

   // table[i][j] holds the minimum weight of a collection of the first i+1 items
   // having at least value j+1
   Cit<Num> currentValue (values.cbegin() + 1);
   Cit<Num> currentWeight (weights.cbegin() + 1);
   Num weightTakingItem;
   for (size_t i = 1; i < numOfRows; ++i, ++currentWeight, ++currentValue) {
      for (size_t j = 0; j < numOfCols; ++j) {
         weightTakingItem = *currentWeight;
         if (j >= *currentValue) {
            weightTakingItem += table[i-1][j - *currentValue];
         }

         if (table[i-1][j] >= weightTakingItem) {
            // We take item i
            table[i][j] = weightTakingItem;
         } else {
            // We don't take item i
            table[i][j] = table[i-1][j];
         }
      }
   }
   
   // read solution
   
   // check where to start:
   // look for the heighest weight <= weightLimit
   int i (numOfRows - 1);
   int j (numOfCols - 1);
   while (table[i][j] > weightLimit) {
      --j;
   }

   // sort out cases where we can't take any item
   if (j < 0) {
      return std::make_tuple(0, std::vector<int> ());
   }

   // Backtrack solution
   Vec<int> solution;
   Num solutionWeight (table[i][j]);
   solution.reserve(i);
   // we have to diminish i in both of the possible cases -> put it in the loop
   for (; i != 0; --i) {
      // Which other entry did we use to compute this entry?
      if (table[i][j] != table[i-1][j]) {
         // We took item i!
         solution.push_back(i);
         if (j >= values[i]) {
            // The item we take leaves room for other items aswell
            j -= values[i];
         } else {
            // The item we take doesn't leave any room for other items,
            // the knapsack is as full as it gets.
            break;
         }

      }
   }

   // Shall we take in the first item?
   if (j >= 0 && table[0][j] != inf) {
      solution.push_back(0);
   }

   return std::make_tuple(solutionWeight, solution);
}

