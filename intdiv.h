#include <stdbool.h>

static inline int roundedDiv(const int numerator, const int denominator)
{
    const bool resultIsNonnegative = (numerator >= 0) == (denominator > 0);
    const int signForAddition = resultIsNonnegative ? +1 : -1;

    return (2*numerator + signForAddition * denominator) / (2 * denominator);
}