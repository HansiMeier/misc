from dolfin import *

# mesh
n = 200
mesh = UnitSquareMesh(n, n)

# FEM type
typeOfFunctionSpace = 'CG' # Lagrange (Continuous Galerkin)
degree = 2 # Quadratic
femSpace = FunctionSpace(mesh, typeOfFunctionSpace, degree)


# Wave eqn: -c^2 * laplace(u) + d^2/dt^2 u = -source function f
dirichletData = Constant(0) # Zero on the boundary
sourceData = lambda time: Constant(0)
speedOfPropagation = 1.0
initialElongation = Expression('sin(x[0]*x[1])')
initialVelocity = Constant(speedOfPropagation)
boundaryCondition = DirichletBC(V, dirichletData, lambda x, onBoundary: onBoundary)



# variational formulation
test = TestFunction(femSpace)
elongation = TrialFunction(femSpace)
massMatrix = assemble(test * elongation * dx)

stiffnessMatrix = assemble(inner(nabla_grad(test), nabla_grad(elongation)) * dx)
timeDependentSourceForm = lambda time: -sourceData(time) * test * dx


# temporal discretization via stroemer's method
# u_next = 2*u_current - u_previous + timestep^2 * a_current
# a_current = c^2 * laplace(u)_current - f
timestep = 1e-6
time = 0.0
finalTime = 1.0
previousElongation = project(initialElongation, femSpace).vector()
currentElongation = project(initialElongation + timestep * initialVelocity).vector()

# timestepping loop
unknown = Function(v)
while time < finalTime:
    dirichletData.t = time
    sourceVector = assemble(timeDependentSourceForm(time))
    rhsVector = timestep ** 2 * A * currentElongation - sourceVector
    boundaryCondition.apply(massMatrix, rhsVector)
    solve(M, unknown.vector(), rhsVector)
    nextElongation = 2.0 * currentElongation - previousElongation + unknown.vector()
    previousElongation.assign(currentElongation)
    currentElongation.assign(nextElongation)
    time += timestep

plot(currentElongation, interactive=True) 
