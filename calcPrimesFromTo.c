#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h> // for uint64_t
#include <inttypes.h> // for the printing specifiers
#include <stdio.h>

#define _ISOC99_SOURCE
#if __STDC_VERSION__ < 199901L
   #error "Requires C99-compliant compiler!"
#endif

#ifndef NDEBUG
   #define assert(cond) \
      if (!(cond)) { \
         fprintf( \
            stderr, \
            "Assert \"%s\" failed in file \"%s\", line %d, function \"%s\"!\n", \
            #cond, \
            __FILE__, \
            __LINE__, \
            __func__ \
         ); \
         fflush(stderr); \
         abort(); \
      }
#else
   #define assert(cond)
#endif


/**************************************************************************************************
 *                                             EXPMOD                                             *
 **************************************************************************************************/

// modular exponentiation by squaring
uint64_t expmod(uint64_t base, uint64_t exponent, uint64_t mod) {
   base %= mod;
   uint64_t result = 1;
   uint64_t currentPower = base;

   for (; exponent; exponent /= 2) {
      result %= mod;
      if (exponent % 2 == 1) {
         result *= currentPower;
         result %= mod;
      }
      currentPower *= currentPower;
      currentPower %= mod;
   }

   return result;
}


/**************************************************************************************************
 *                                       MILLER-RABIN TEST                                        *
 **************************************************************************************************/

bool millerRabinPrime(const uint64_t n) {

   // The witnesses to check
   static const uint64_t witnessesTiny[2] = {2, 3};
   static const uint64_t* const endTiny = witnessesTiny + 2;
   const uint64_t thresholdTiny = UINT64_C(1373652);
   
   static const uint64_t witnessesSmall[2] = {31, 37};
   static const uint64_t* const endSmall = witnessesSmall + 2;
   const uint64_t thresholdSmall = UINT64_C(9080190);
   
   static const uint64_t witnessesMedium[3] = {2, 7, 61};
   static const uint64_t* const endMedium = witnessesMedium + 3;
   const uint64_t thresholdMedium = UINT64_C(4759123140);
   
   static const uint64_t witnessesBig[5] = {2, 3, 5, 7, 11};
   static const uint64_t* const endBig = witnessesBig + 5;
   const uint64_t thresholdBig = UINT64_C(2152302898746);
   
   static const uint64_t witnessesHuge[6] = {2, 3, 5, 7, 11, 13};
   static const uint64_t* const endHuge = witnessesHuge + 6;
   const uint64_t thresholdHuge = UINT64_C(3474749660382);
   
   static const uint64_t witnessesGigantic[7] = {2, 3, 5, 7, 11, 13, 17};
   static const uint64_t* const endGigantic = witnessesGigantic + 7;
   const uint64_t thresholdGigantic = UINT64_C(341550071728320);
   
   static const uint64_t witnessesColossal[9] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
   static const uint64_t* const endColossal = witnessesColossal + 9;
   const uint64_t thresholdColossal = UINT64_MAX;
   
   const uint64_t* witnesses;
   const uint64_t* end;
   
   // select which witnesses to check
   if (n <= thresholdTiny) {
   	witnesses = witnessesTiny;
   	end = endTiny;
   } else if (n <= thresholdSmall) {
   	witnesses = witnessesSmall;
   	end = endSmall;
   } else if (n <= thresholdMedium) {
   	witnesses = witnessesMedium;
   	end = endMedium;
   } else if (n <= thresholdBig) {
   	witnesses = witnessesBig;
   	end = endBig;
   } else if (n <= thresholdHuge) {
   	witnesses = witnessesHuge;
   	end = endHuge;
   } else if (n <= thresholdGigantic) {
   	witnesses = witnessesGigantic;
   	end = endGigantic;
   } else if (n <= thresholdColossal) {
   	witnesses = witnessesColossal;
   	end = endColossal;
   } else {
      // should never be here!
      assert(!"Don't know which witnesses to check!");
   }


   uint64_t expTwo = 0; // n == 2^expTwo * oddPart
   uint64_t oddPart = n - 1;
   for (; oddPart % 2 == 0; oddPart /= 2, ++expTwo);

   // Witness loop
   const uint64_t nMinusOne = n - 1;
   uint64_t rest;
   bool doNextWitness;
   for (const uint64_t* witness = witnesses; witness != end && *witness < nMinusOne; ++witness) {
      rest = expmod(*witness, oddPart, n);

      if (rest == 1 || rest == nMinusOne) {
         continue;
      }

      doNextWitness = false;

      for (uint64_t count = 1; count < expTwo; ++count) {
         rest = rest*rest % n;
         if (rest == 1) {
            return false;
         } else if (rest == nMinusOne) {
            doNextWitness = true;
            break;
         }
      }

      if (!doNextWitness) {
         return false;
      }
   }

   return true;
}

int main() {

   uint32_t from;
   printf("From? ");
   scanf("%u", &from);
   uint32_t realFrom = from;

   if (from < 2) {
      from = 2;
   }

   from += 1 - from%2;
   assert(from > 1 && from%2);

   uint32_t to;
   printf("To? ");
   scanf("%u", &to);
   uint32_t realTo = to;
   to -= 1 - to%2;
   assert (to >= from && to%2);

   const size_t filenameSize = 200;
   char* filename = malloc(filenameSize * sizeof(char));
   assert(filename);
   snprintf(filename, filenameSize, "primes from %" PRIu32 " to %" PRIu32 " as UINT32.dat", realFrom, realTo);
   printf("Saving to file \"%s\".\n", filename);
   FILE* fp = fopen(filename, "wb");
   assert(fp);

   uint32_t maybePrime = 2;
   if (realFrom < 3) {
      fwrite(&maybePrime, sizeof(uint32_t), 1, fp);
      assert(!ferror(fp));
   }

   for (maybePrime = from; maybePrime <= to; maybePrime += 2) {
      if (millerRabinPrime(maybePrime)) {
         fwrite(&maybePrime, sizeof(uint32_t), 1, fp);
         assert(!ferror(fp));
      }
   }

   assert(!fclose(fp)); // fclose returns zero on success
   free(filename);
   return 0;
}
