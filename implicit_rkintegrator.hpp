#ifndef implicit_rkintegrator_hpp
#define implicit_rkintegrator_hhp

#include <vector>
#include "myAssert.hpp"
#include <iostream>
#include <iomanip>
#include <Eigen/Dense>
#include <unsupported/Eigen/KroneckerProduct>
#include "dampnewton.hpp"

//! \file implicit_rkintegrator.hpp Solution for Problem 1, implementing implicit_RkIntegrator class
#define kron(A, B) Eigen::kroneckerProduct(A, B)


//! \brief Implements a Runge-Kutta implicit solver for a given Butcher tableau for autonomous ODEs
using ConstVectorRef = const Eigen::Ref<const Eigen::VectorXd>&;
using VectorRef = Eigen::Ref<Eigen::VectorXd>;
using ConstMatrixRef = const Eigen::Ref<const Eigen::MatrixXd>&;
class implicit_RKIntegrator {
   public:

      //! \brief Constructor for the implicit RK method.
      //! Performs size checks and copies A and b into internal storage
      //! \param[in] A matrix containing coefficents of Butcher tableau, must be (strictly) lower triangular (no check)
      //! \param[in] b vector containing coefficients of lower part of Butcher tableau
      implicit_RKIntegrator(ConstMatrixRef A, ConstVectorRef b)
         : A(A), b(b), s(b.size()) {
         assert(A.cols() == A.rows() && "Matrix must be square.");
         assert(A.cols() == b.size() && "Incompatible matrix/vector size.");
      }

      //! \brief Perform the solution of the ODE
      //! Solve an autonomous ODE y' = f(y), y(0) = y0, using an implicit RK scheme given in the Butcher tableau provided in the
      //! constructor. Performs N equidistant steps upto time T with initial data y0
      //! \tparam Function type for function implementing the rhs function. Must have Eigen::VectorXd operator()(Eigen::VectorXd x)
      //! \tparam Function2 type for function implementing the Jacobian of f. Must have Eigen::MatrixXd operator()(Eigen::VectorXd x)
      //! \param[in] f function handle for rhs in y' = f(y), e.g. implemented using lambda funciton
      //! \param[in] Jf function handle for Jf, e.g. implemented using lambda funciton
      //! \param[in] T final time T
      //! \param[in] y0 initial data y(0) = y0 for y' = f(y)
      //! \param[in] N number of steps to perform. Step size is h = T / N. Steps are equidistant.
      //! \return vector containing all steps y^n (for each n) including initial and final value
      template <class Function, class Function2>
      std::vector<Eigen::VectorXd> solve(const Function& f, const Function2& Jf, double T, ConstVectorRef y0, unsigned int N) const {
         // Iniz step size
         double h = T / N;

         // Will contain all steps, reserve memory for efficiency
         std::vector<Eigen::VectorXd> res;
         res.reserve(N + 1);

         // Store initial data
         res.push_back(y0);

         // Initialize some memory to store temporary values
         Eigen::VectorXd ytemp1 = y0;
         Eigen::VectorXd ytemp2 = y0;
         // Pointers to swap previous value
         Eigen::VectorXd* yold = &ytemp1;
         Eigen::VectorXd* ynew = &ytemp2;

         // Loop over all fixed steps
         for (unsigned int k = 0; k < N; ++k) {
            // Compute, save and swap next step
            step(f, Jf, h, *yold, *ynew);
            res.push_back(*ynew);
            std::swap(yold, ynew);
         }
         return res;
      }

   private:

      //! \brief Perform a single step of the RK method for the solution of the autonomous ODE
      //! Compute a single explicit RK step y^{n+1} = y_n + \sum ... starting from value y0 and storing next value in y1
      //! \tparam Function type for function implementing the rhs. Must have Eigen::VectorXd operator()(Eigen::VectorXd x)
      //! \tparam Function2 type for function implementing the Jacobian of f. Must have Eigen::MatrixXd operator()(Eigen::VectorXd x)
      //! \param[in] f function handle for ths f, s.t. y' = f(y)
      //! \param[in] Jf function handle for Jf, e.g. implemented using lambda funciton
      //! \param[in] h step size
      //! \param[in] y0 initial Eigen::VectorXd
      //! \param[out] y1 next step y^{n+1} = y^n + ...
      using Index = Eigen::VectorXd::Index;
      template <class Function, class Function2>
      void step(const Function& f, const Function2& Jf, const double h, ConstVectorRef y0, VectorRef y1) const {
         // TODO: implement a single step of the implicit RK method using provided Butcher scheme. Use the damped Newton method to find the stages g as zeros of a non-linear system of equations.
         const Index d(y0.size());
         const Eigen::MatrixXd hAkronI(h * kron(A, Eigen::MatrixXd::Identity(d, d)));
         assert(hAkronI.rows() == hAkronI.cols() && hAkronI.cols() == s*d);
         // F(g) = g - h * (A kron I_d)*ff
         // where
         // - g s*d column vector, g = (g_1, ..., g_s).transpose()
         // - g_i d column vector
         // - A s x s matrix
         // - I_d d x d identity matrix
         // - ff = (f(y0 + g_1), f(y0 + g_2), ..., f(y0 + g_s)).transpose()
         // - f a function from R^d to R^d
         // - y0 a d column vector
         const auto F([this, &f, &d, &hAkronI, &y0] (ConstVectorRef g) -> Eigen::VectorXd {
            precond(s * d == g.size());

            Eigen::VectorXd ff(s*d);
            for (Index i(0); i < s; ++i) {
               ff.segment(i*d, d) = f(y0 + g.segment(i*d, d));
            }

            return g - hAkronI * ff;
         });

         const auto DF([this, &Jf, &d, &hAkronI, &y0] (ConstVectorRef g) -> Eigen::MatrixXd {
            precond(s * d == g.size());
            Eigen::MatrixXd blockJacobian(Eigen::MatrixXd::Zero(s*d, s*d));

            for (Index i(0); i < s; ++i) {
               blockJacobian.block(i*d, i*d, d, d) = Jf(y0 + g.segment(i*d, d));
            }

            return Eigen::MatrixXd::Identity(s*d, s*d) - hAkronI * blockJacobian;
         });

         Eigen::VectorXd g(Eigen::VectorXd::Zero(s*d));
         dampnewton(F, DF, g);
         y1 = y0;
         for (Index i(0); i < s; ++i) {
            y1 += h * b(i) * f(y0 + g.segment(i*d, d));
         }

         return;
      }


      //! Matrix A in Butcher scheme
      const Eigen::MatrixXd A;
      //! Vector b in Butcher scheme
      const Eigen::VectorXd b;
      //! Size of Butcher matrix and vector A and b
      const unsigned int s;
};

#endif

