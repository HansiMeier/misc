#include "fifoLock.hpp"

FifoLock::FifoLock(
):
    numberToAssign_M(0),
    numberToServe_M(0) {
}

void
FifoLock::lock(
) {
    const int myNumber(numberToAssign_M.fetch_add(1, std::memory_order_relaxed));
    const auto isItReallyMyTurn(
        [myNumber, this]
        () {
            const int currentlyServed(numberToServe_M.load(std::memory_order_relaxed));
            return currentlyServed == myNumber;

        }
    );
   
    forMyTurn_M.wait(turnLock_M);
    for (;;) {
        if (isItReallyMyTurn()) {
            return;
        } else {
            // spurious wakeup
            forMyTurn_M.notify_one();
            forMyTurn_M.wait(turnLock_M);
        }
    }
}

void
FifoLock::unlock(
) {
    (void) numberToServe_M.fetch_add(1, std::memory_order_relaxed);
    return forMyTurn_M.notify_one();
}
