function nBodies(X)
    filename = 'nBodies.mat';
    if nargin < 1
        S = whos('-file', filename);
        m = matfile(filename);
        X = m.(S.name);
    end
    [endInd, loops] = size(X);
     n = floor(endInd/3);
    colormap(parula(n));
    colors = transpose(1:n);
    radius = 50;
    v = VideoWriter('nBodies.mp4', 'MPEG-4');
    open(v);
    
    for i=1:loops
        xs=X(1:3:endInd,i);
        ys=X(2:3:endInd,i);
        zs=X(3:3:endInd,i);
        scatter3(xs, ys, zs, radius, colors, 'filled')

        ax=gca;
        grid(ax, 'off');
        frame=getframe(gcf);
        writeVideo(v, frame);
    end
    close(v);
end
