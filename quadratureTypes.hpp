#pragma once
#include <Eigen/Dense>
#include <functional>
#include <type_traits>


template <int dimension>
using Vector = Eigen::Matrix<double, dimension, 1>;

template <int dimension, typename Derived>
struct CheckedVectorExpression {
    static_assert(dimension == 1 || std::is_convertible<Derived, Vector<dimension>>::value, "Vector expression not convertible!");
    using type = Derived;
};

template <int dimension, typename Derived = Vector<dimension>>
using VectorExpression = typename CheckedVectorExpression<dimension, Derived>::type;

template <int dimension, typename Derived = Vector<dimension>>
using RealFunction = typename std::conditional<dimension == 1, double(double), double(VectorExpression<dimension, Derived>)>::type;

template <int dimension, typename Derived = Vector<dimension>>
using Quadrature = double(const RealFunction<dimension, Derived>);
