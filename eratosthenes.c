#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>

#ifndef NDEBUG
#define assert(cond) \
      if (!(cond)) { \
         fprintf( \
            stderr, \
            "Assert \"%s\" failed in file \"%s\", line %d, function \"%s\"!\n", \
            #cond, \
            __FILE__, \
            __LINE__, \
            __func__ \
         ); \
         fflush(stderr); \
         abort(); \
      }
#else
#define assert(cond)
#endif

typedef uint32_t uint;

uint* makeTableOfOdds(uint from, uint to, uint* sizep) {
   assert(from <= to);

   from += (1 - from % 2); // make from odd
   to -= (1- to % 2); // make to odd

   *sizep = (to - from + 1) / 2;
   uint* table = malloc(*sizep * sizeof(uint));
   if (!table) {
      return NULL;
   }

   for (uint* entry = table; from <= to; from += 2, ++entry) {
      *entry = from;
   }

   return table;
}

// The table is freed in any case
void removeZerosAndAddTwo(uint** tablep, uint* sizep, const uint crossedOut) {
   const uint oldSize = *sizep;
   *sizep -= crossedOut;

   uint* table = *tablep;

   uint* primes = malloc(sizeof(uint) * (1 + *sizep));
   if (!primes) {
      free(table);
      *tablep = NULL;
      return;
   }

   // copy table to a smaller one without all the 0s
   const uint* end = table + oldSize;
   uint* writep = primes;
   *writep++ = 2;
   for (const uint* readp = table; readp != end; ++readp) {
      if (*readp) {
         *writep++ = *readp;
      }
   }

   free(table);
   *tablep = primes;

   return;
}

void crossOut(uint* table, const uint* tableEnd, uint* crossedOutp) {
   assert(table && table < tableEnd);
   // cross out the multiples
   uint crossedOut = 0;
   for (uint* p = table; p != tableEnd && *p; ++p) {
      for (uint* q = p + *p; q < tableEnd; q += *p) {
         if (*q) {
            *q = 0;
            ++crossedOut;
         } else {
            break;
         }
      }
   }

   *crossedOutp = crossedOut;
   return;
}

uint* primesUpTo(uint to, uint* sizep) {
   assert(to >= 3);

   uint tableSize;
   uint* table = makeTableOfOdds(3, to, &tableSize);
   assert(table);
   uint crossedOut;
   crossOut(table, table + tableSize, &crossedOut);
   removeZerosAndAddTwo(&table, &tableSize, crossedOut);

   return table;
}



int main() {
   uint size;
   uint* primes = primesUpTo(2147483647, &size);
   assert(primes);

   FILE* fp = fopen("/home/stefano/primeTable.bin", "wb");
   assert(fp);
   assert(fwrite(primes, size, 1, fp));
   fclose(fp);

   const uint* primesEnd = primes + size;
   for (uint* p = primes; p != primesEnd; ++p) {
      printf("%u ", *p);
   }

   free(primes);
   return 0;
}



