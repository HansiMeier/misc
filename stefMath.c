
/**************************************************************************************************
 *                                      BINOMIAL COEFFICIENT                                      *
 **************************************************************************************************/

unum binCoeff(unum n, unum k) {

#ifdef FAILSAFE_STEFMATH
   if (k > n) {
      return 0;
   }
#endif

   if (k == 0) {
      return 1;
   }

   if (k >= n/2) {
      k = n - k;
   }

   unum factor = n - k + 1;
   unum result = factor;

   for (unum i = 2; i <= k; ++i) {
      ++factor;
      result *= factor;
      result /= i;
   }

   return result;
}

/**************************************************************************************************
 *                                       FALLING FACTORIAL                                        *
 **************************************************************************************************/

unum fallingFact(unum n, unum k) {
#ifdef FAILSAFE_STEFMATH
   if (k > n) {
      return 0;
   }
#endif

   unum factor = n;
   unum product = 1;
   for (unum i = 0; i < k; ++i) {
      product *= factor;
      --factor;
   }

   return product;
}

/**************************************************************************************************
 *                                             EXPMOD                                             *
 **************************************************************************************************/

// modular exponentiation by squaring
unum expmod(unum base, unum exponent, const unum mod) {
   base %= mod;
   unum result = 1;
   unum currentPower = base;

   for (; exponent; exponent /= 2) {
      result %= mod;
      if (exponent % 2 == 1) {
         result *= currentPower;
         result %= mod;
      }
      currentPower *= currentPower;
      currentPower %= mod;
   }

   return result;
}


/**************************************************************************************************
 *                                       MILLER-RABIN TEST                                        *
 **************************************************************************************************/

bool millerRabinPrime(const unum n) {

   // The witnesses to check
   static const unum witnessesTiny[2] = {2, 3};
   static const unum* const endTiny = witnessesTiny + 2;
   const unum thresholdTiny = UINT64_C(1373652);

   static const unum witnessesSmall[2] = {31, 37};
   static const unum* const endSmall = witnessesSmall + 2;
   const unum thresholdSmall = UINT64_C(9080190);

   static const unum witnessesMedium[3] = {2, 7, 61};
   static const unum* const endMedium = witnessesMedium + 3;
   const unum thresholdMedium = UINT64_C(4759123140);

   static const unum witnessesBig[5] = {2, 3, 5, 7, 11};
   static const unum* const endBig = witnessesBig + 5;
   const unum thresholdBig = UINT64_C(2152302898746);

   static const unum witnessesHuge[6] = {2, 3, 5, 7, 11, 13};
   static const unum* const endHuge = witnessesHuge + 6;
   const unum thresholdHuge = UINT64_C(3474749660382);

   static const unum witnessesGigantic[7] = {2, 3, 5, 7, 11, 13, 17};
   static const unum* const endGigantic = witnessesGigantic + 7;
   const unum thresholdGigantic = UINT64_C(341550071728320);

   static const unum witnessesColossal[9] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
   static const unum* const endColossal = witnessesColossal + 9;
   const unum thresholdColossal = UINT64_MAX;

   const unum* witnesses;
   const unum* end;

   // select which witnesses to check
   if (n <= thresholdTiny) {
   	witnesses = witnessesTiny;
   	end = endTiny;
   } else if (n <= thresholdSmall) {
   	witnesses = witnessesSmall;
   	end = endSmall;
   } else if (n <= thresholdMedium) {
   	witnesses = witnessesMedium;
   	end = endMedium;
   } else if (n <= thresholdBig) {
   	witnesses = witnessesBig;
   	end = endBig;
   } else if (n <= thresholdHuge) {
   	witnesses = witnessesHuge;
   	end = endHuge;
   } else if (n <= thresholdGigantic) {
   	witnesses = witnessesGigantic;
   	end = endGigantic;
   } else if (n <= thresholdColossal) {
   	witnesses = witnessesColossal;
   	end = endColossal;
   } else {
      // should never be here!
      assert(!"Don't know which witnesses to check!");
   }

   unum expTwo = 0; // n == 2^expTwo * oddPart
   unum oddPart = n - 1;
   for (; oddPart % 2 == 0; oddPart /= 2, ++expTwo);

   // Witness loop
   const unum nMinusOne = n - 1;
   unum rest;
   bool doNextWitness;
   for (const unum* witness = witnesses; witness != end && *witness < nMinusOne; ++witness) {
      rest = expmod(*witness, oddPart, n);

      if (rest == 1 || rest == nMinusOne) {
         continue;
      }

      doNextWitness = false;

      for (unum count = 1; count < expTwo; ++count) {
         rest = rest*rest % n;
         if (rest == 1) {
            return false;
         } else if (rest == nMinusOne) {
            doNextWitness = true;
            break;
         }
      }

      if (!doNextWitness) {
         return false;
      }
   }

   return true;
}

/**************************************************************************************************
 *                                   BEST RATIONAL APPROXIMANT                                    *
 **************************************************************************************************/

static void bestRationalApproximantForUnitInterval(const rnum toApproximate, const unum maximalDenominator, const rnum relativeTolerance, inum* result) {
    assert(toApproximate >= 0.0 && toApproximate <= 1.0);
    assert(result != NULL);
    assert(maximalDenominator > 0);


    // lowerNum / lowerDen and upperNum / upperDen are bounds to the binary search below
    unum lowerNum = 0;
    unum lowerDen = 1;
    unum upperNum = 1;
    unum upperDen = 1;

    while (lowerDen <= maximalDenominator && upperDen <= maximalDenominator) {
        const unum mediantDen = lowerDen + upperDen;
        const unum mediantNum = lowerNum + upperNum;

        // guaranteed to lie within lowerNum/lowerDen and upperNum/upperDen
        const rnum mediant = ((rnum) (mediantNum)) / ((rnum) (mediantDen));
        if (toApproximate > mediant) {
            lowerDen = mediantDen;
            lowerNum = mediantNum;
        } else if (toApproximate < mediant) {
            upperDen = mediantDen;
            upperNum = mediantNum;
        } else if (fabs(mediant - toApproximate) <= relativeTolerance * toApproximate) {
            if (mediantDen <= maximalDenominator) {
                // mediant is best choice
                result[0] = mediantNum;
                result[1] = mediantDen;
            } else if (upperDen > lowerDen) {
                // upper bound is best choice
                result[0] = upperNum;
                result[1] = upperDen;
            } else {
                // lower bound is best choice
                result[0] = lowerNum;
                result[1] = lowerDen;
            }
            return;
        }
    }

    if (lowerDen <= maximalDenominator) {
        // lower bound is best choice
        result[0] = lowerNum;
        result[1] = lowerDen;
    } else {
        // upper bound is best choice
        result[0] = upperNum;
        result[1] = upperDen;
    }
}

void bestRationalApproximant(rnum toApproximate, const unum maximalDenominator, const rnum relativeTolerance, inum* result) {
    inum sign = 1;
    if (toApproximate < 0.0) {
        toApproximate *= -1.0;
        sign *= -1;
    }

    assert(toApproximate >= 0.0);
    unum wholePart = 0;
    if (toApproximate >= 1.0) {
        wholePart = (unum) toApproximate;
        toApproximate -= (rnum) wholePart;
    }

    assert(toApproximate >= 0.0 && toApproximate <= 1.0);

    bestRationalApproximantForUnitInterval(toApproximate, maximalDenominator, relativeTolerance, result);

    result[0] = sign * (result[1] * wholePart + result[0]);
}
