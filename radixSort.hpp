#include <vector>
#include <cstdint>

/**************************************************************************************************
 *                           RADIX SORT FOR VECTORS OF INTS AND DOUBLES                           *
 **************************************************************************************************/

void radixSort(std::vector<double>& toSort);
void radixSort(std::vector<int64_t>& toSort);

#include "radixSort.cpp"

