#define IntNum int64_t
#define FlipSignBitMask 0x8000000000000000LU
#define RawBits uint64_t
void radixSortLongInternal(std::vector<RawBits>& v) {
   size_t vSize (v.size());
   RawBits* buckets (new RawBits[2 * vSize]);


   // Handle negative values too:
   // Massage the numbers to sort them (decorate):
   for (RawBits& n: v) {
      n ^= FlipSignBitMask;
   }

   RawBits** current (new RawBits*[2]);
   typename std::vector<RawBits>::iterator it;
   for (size_t i (0); i < sizeof(RawBits) * 8; ++i) {
      // Sort into buckets
      current[0] = buckets;
      current[1] = buckets + vSize;
      for (RawBits n: v) {
         size_t index ((n >> i) & 1);
         *(current[index]++) = n;
      }

      // Collect 0s
      it = v.begin();
      for (RawBits* c0 (buckets); c0 != current[0]; ++c0, ++it) {
         *it = *c0;
      }

      // Collect 1s
      for (RawBits* c1 (buckets + vSize); c1 != current[1]; ++c1, ++it) {
         *it = *c1;
      }
   }

   // undecorate the numbers
   for (RawBits& n: v) {
      n ^= FlipSignBitMask;
   }

   delete[] current;
   delete[] buckets;
   return;
}

void radixSort (std::vector<IntNum>& v) {
   std::vector<RawBits>* p (reinterpret_cast<std::vector<RawBits>*>(&v));
   return radixSortLongInternal(*p);
}

#undef IntNum


#define FloatNum double
static void radixSortDoubleInternal(std::vector<RawBits>& v) {
   size_t vSize (v.size());
   RawBits* buckets (new RawBits[2 * vSize]);

   // Handle negative values too:
   // Massage the numbers to sort them (decorate):
   for (RawBits& n: v) {
      if (n < FlipSignBitMask) {
         n ^= FlipSignBitMask;
      } else {
         n = ~n;
      }
   }

   RawBits** current (new RawBits*[2]);
   typename std::vector<RawBits>::iterator it;
   for (size_t i (0); i < sizeof(RawBits) * 8; ++i) {
      // Sort into buckets
      current[0] = buckets;
      current[1] = buckets + vSize;
      for (RawBits n: v) {
         size_t index ((n >> i) & 1);
         *(current[index]++) = n;
      }

      // Collect 0s
      it = v.begin();
      for (RawBits* c0 (buckets); c0 != current[0]; ++c0, ++it) {
         *it = *c0;
      }

      // Collect 1s
      for (RawBits* c1 (buckets + vSize); c1 != current[1]; ++c1, ++it) {
         *it = *c1;
      }
   }

   // undecorate the numbers
   for (RawBits& n: v) {
      if (n < FlipSignBitMask) {
         n = ~n;
      } else {
         n ^= FlipSignBitMask;
      }
   }

   delete[] current;
   delete[] buckets;
   return;
}

void radixSort(std::vector<FloatNum>& v) {
   std::vector<RawBits>* p (reinterpret_cast<std::vector<RawBits>*>(&v));
   return radixSortDoubleInternal(*p);
}
#undef FloatNum
#undef FlipSignBitMask
#undef RawBits

