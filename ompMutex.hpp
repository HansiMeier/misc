#pragma once
#include <omp.h>

/**************************************************************************************************
 *                              OPENMP MUTEX AS C++11 MUTEX CONCEPT                               *
 **************************************************************************************************/

class OmpMutex {
    public:
        inline
        OmpMutex(
        ) {
            omp_init_lock(&lock_M);
        }

        inline
        OmpMutex(
            const OmpMutex&
        ) = delete;

        inline
        OmpMutex(
            OmpMutex&&
        ) = delete;

        inline
        ~OmpMutex(
        ) {
            omp_destroy_lock(&lock_M);
        }

        inline
        void
        lock(
        ) {
            omp_set_lock(&lock_M);
        }

        inline
        void
        unlock(
        ) {
            omp_unset_lock(&lock_M);
        }

        inline
        bool
        try_lock(
        ) {
            return bool(omp_test_lock(&lock_M));
        }

    private:
        omp_lock_t lock_M;
};

