#include <cmath>
#include <iostream>
#include <iomanip>
#include "myAssert.hpp"
#include <limits>

double myExp(double x) {
   precond(x >= 0);
   const int thresholdExponent(-7);
   const double ln2(M_LN2);
   const double xMax(std::log(std::numeric_limits<double>::max()));

   if (x >= xMax) {
      return 0;
   }

   int shift(x / ln2);
   x = std::fmod(x, ln2);

   int exponent;
   const double mantissa(std::frexp(x, &exponent));

   exponent -= thresholdExponent;
   x = std::ldexp(mantissa, thresholdExponent);

   x = 1 + x * (1 + x * (0.5 + x * (1.0/6.0 + x * (1.0/24.0 + x * (1.0/120.0 + x/720.0)))));

   for (; exponent > 0; --exponent) {
      x *= x;
   }

   for (; shift > 0; --shift) {
      x *= 2;
   }

   return x;

}

int main() {
   double x;
   std::cout << "x =? ";
   std::cin >> x;
   std::cout << std::setprecision(16) << "myexp(x) = " << myExp(x) << "\ncmexp(x) = " << std::exp(x) << "\n";

   return 0;
}

