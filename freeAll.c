#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#pragma once
static inline void freeAndSetToNull(void* *const toFree) {
    if (toFree == NULL || *toFree == NULL) {
        return;
    }

    free(*toFree);
    *toFree = NULL;
}

static inline void freeAllAndSetToNull(const int numberOfPointers, ...) {
    va_list args;
    va_start(args, numberOfPointers);

    for (int pointerIndex = 0; pointerIndex < numberOfPointers; ++pointerIndex) {
        void* *const toFree = va_arg(args, void* *const);
        freeAndSetToNull(toFree);
    }

    va_end(args);
}


#ifdef FREE_ALL_TEST
int main() {
    double* d = malloc(4 * sizeof(double));

    void* *const testPtrConversion = (void* *const) &d;
    freeAllAndSetToNull(1, &d);

    if (d != NULL) {
        fputs("Pointer nonNULL", stderr);
        abort();
    }
    return 0;
}
#endif
