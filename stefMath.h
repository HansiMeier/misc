/**************************************************************************************************
<<<<<<< HEAD
 *                                            STEFMATH                                            *
=======
 *															 STEFMATH							  *
>>>>>>> f924bda81adf862c92c4e4230381eda19a05de33
 **************************************************************************************************/

#ifndef STEFMATH
#define STEFMATH

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h> // for uint64_t
#include <inttypes.h> // for the printing specifiers
#include <stdio.h>
#include <tgmath.h>

#define _ISOC99_SOURCE
#if __STDC_VERSION__ < 199901L
   #error "Requires C99-compliant compiler!"
#endif

#ifndef NDEBUG
   #define assert(cond) \
      if (!(cond)) { \
         fprintf( \
            stderr, \
            "Assert \"%s\" failed in file \"%s\", line %d, function \"%s\"!\n", \
            #cond, \
            __FILE__, \
            __LINE__, \
            __func__ \
         ); \
         fflush(stderr); \
         abort(); \
      }
#else
   #define assert(cond)
#endif

typedef intmax_t inum;
typedef uintmax_t unum;
typedef double rnum;

// modular exponentiation by squaring
unum expmod(unum base, unum exponent, unum mod);

// binomial coefficient
unum binCoeff(unum n, unum k);

// falling factorial
unum fallingFact(unum n, unum k);

// prime test by miller rabin
bool millerRabinPrime(const unum n);

// Farey-sequence finding the best rational appro, ximant
// needs pointer to array of two unums, the numerator and denominator
// of the best rational approximant
// the denominantor is always positive: -0.25 -> {-1, 4}
void bestRationalApproximant(rnum toApproximate, const unum maximalDenominator, const rnum relativeTolerance, inum* result);

#include "stefMath.c"
#endif

