#include <cmath>
#include <stdexcept>
#include <limits>
#include <type_traits>


namespace modifiedNewton {
   template <typename T>
   using IsFloatingType = typename std::enable_if<std::numeric_limits<T>::is_iec559, T>::type;

   template <typename T>
   using IsIntegerType = typename std::enable_if<std::numeric_limits<T>::is_integer, T>::type;


   template <typename Scalar, typename Function, typename Jacobian>
   Scalar step(typename IsFloatingType<Scalar>::type x,
               const Function& f,
               const Jacobian& Df,
               const Scalar consideredZero = 1e-16) {
      const Scalar fAtX(f(x));

      const Scalar DfAtX(Df(x));
      if (std::abs(DfAtX) <= consideredZero) {
         throw std::runtime_error("modified Newton: Jacobian nearly singular!");
      }

      const Scalar y(x + fAtX / DfAtX);
      x = y - f(y) / DfAtX;
      return x;
   }

   template <typename Scalar, typename Index, typename Function, typename Jacobian>
   Scalar fzero(IsFloatingType<Scalar> x,
                const Function& f,
                const Jacobian& Df,
                const Scalar relTol = 1e-8,
                const Scalar absTol = 1e-16,
                const IsIntegerType<Index> maxNumSteps = 1000) {

      for (Index n(0); n < maxNumSteps; ++n) {
         const Scalar xNew(step(x, f, Df));
         const Scalar change(std::abs(xNew - x));

         if (change < absTol || change < relTol * std::abs(x)) {
            return xNew;
         }

         x = xNew;
      }

      throw std::runtime_error("modified Newton: Didn't converge!");
   }

}
