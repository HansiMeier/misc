#include <Eigen/Dense>
#include "myAssert.hpp"
#include <iostream>
#include "EigenMATWriter.hpp"
#include <cmath>

#ifndef NUM_BODIES
#define NUM_BODIES 3
#endif

using Vector = Eigen::VectorXd;
using ConstVectorRef = const Eigen::Ref<const Vector>&;
using VectorRef = Eigen::Ref<Vector>;
using States = Eigen::MatrixXd;
using Index = Vector::Index;

Vector newtonAccelerations(ConstVectorRef positions, ConstVectorRef masses) {
   const Index n(masses.size());
   precond(positions.size() == 3 * n, n > 0);

   Vector accels(Vector::Zero(3*n));
   Vector distanceVectorItoJ(3);
   double squaredDistance;
   for (Index i(1); i < n; ++i) {
      for (Index j(0); j < i; ++j) {
         distanceVectorItoJ = positions.segment(3*j, 3) - positions.segment(3*i, 3);
         squaredDistance = distanceVectorItoJ.squaredNorm();
         distanceVectorItoJ.normalize();

         accels.segment(3*i, 3) += masses(j) / squaredDistance * distanceVectorItoJ;
         accels.segment(3*j, 3) -= masses(i) / squaredDistance * distanceVectorItoJ;
      }
   }
   const double newtonsConstant(6.67408e-11);
   postcond(accels.size() == positions.size());
   std::cout  << "ACCEL = \n\n" << accels << "\n\n";
   return accels *= newtonsConstant;
}

States nBodies(ConstVectorRef masses, ConstVectorRef initialPositions,
               ConstVectorRef initialVelocities, double stepSize,
               const Index numStatesToCalc) {
   precond(stepSize > 0, numStatesToCalc > 0, masses.size() > 0,
           masses.size() * 3 == initialVelocities.size(),
           initialVelocities.size() == initialPositions.size());
   const Index n(masses.size());


   // use Verlet's integrator
   States positions(3*n, numStatesToCalc + 1);
   positions.col(0) = initialPositions;
   positions.col(1) = initialPositions
                  + stepSize * initialVelocities
                  + stepSize*stepSize/2 * newtonAccelerations(initialPositions, masses);
   std::cout << "POS = \n\n" << positions.col(1) <<"\n";
   for (Index i(2); i < numStatesToCalc + 1; ++i) {
      positions.col(i) = 2 * positions.col(i-1)
                         - positions.col(i-2)
                         + stepSize * stepSize * newtonAccelerations(positions.col(i-1), masses);
      std::cout << "POS = \n\n" << positions.col(i) << "\n\n";
   }



   return positions;
}

Vector normalizeTo(ConstVectorRef v, const double norm) {
   precond(norm >= 0);
   Vector vNorm(v);
   vNorm.normalize();
   vNorm *= norm;
   return vNorm;
}

int main() {
   const Index n(3);
   #undef NUM_BODIES
   Vector masses(n);
   masses << 2e10, 2e10, 2e16;
   const double positionsNorm(100);
   const double velocitiesNorm(10);
   const Vector initialPositions(normalizeTo(Vector::Random(3*n), positionsNorm));
   const Vector initialVelocities(normalizeTo(Vector::Random(3*n), velocitiesNorm));
   const double stepSize(0.05);
   const Index numStatesToCalc(10000);

   States positionsOverTime(
      nBodies(
         masses, initialPositions,
         initialVelocities, stepSize,
         numStatesToCalc
      )
   );

   EigenMATWriter mat("nBodies.mat");
   mat.add("P", positionsOverTime);

   return 0;
}
   

