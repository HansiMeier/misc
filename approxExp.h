#pragma once
#include <math.h>


const static int approxExp_N = 8192;
const static int approxExp_BinaryLogOfN = 13;


static inline
double
approxExpForNegative(
    const double x
) {
    /* approximate it with the compound interest limit
     * exp(x) = limit as n goes to infinity of (1 + x/n)^n
     * with n a suitable power of two
     */

     double toBeExpX = 1.0 + x / approxExp_N;

     int squaringTime;
     for (squaringTime = 0; squaringTime < approxExp_BinaryLogOfN; ++squaringTime) {
         toBeExpX *= toBeExpX;
     }

     return toBeExpX;
 }

static inline
double
approxExp(
    const double x
) {
    if (fabs(x) > approxExp_N) {
        return exp(x);
    } else if (x > 0) {
        return 1.0 / exp(-x);
    } else {
        return approxExpForNegative(x);
    }
 }
