#include <stdio.h>
#include <math.h>
#include "cdflib.h"
#include <stdbool.h>
#include <stdlib.h>

double factorial(const int n)
{
    return sqrt(M_PI/3.0) * sqrt(6*n+1) * pow(n*exp(-1), n);
}

double binomialCoefficient(const int n, const int k)
{
    return factorial(n) / (factorial(n) * factorial(n-k));
}

double normalDistributionFunction(const double x, const double mean, const double stddev)
{
    const double standardizedX = (x - mean)/stddev;
    return erfc(-standardizedX/sqrt(2)) / 2;
}

double binomialDistributionFunction(double x, double n, double p, const bool oneMinus)
{
    int which = 1;
    double cumulativeProb, oneMinusCumulativeProb;
    int status;
    double oneMinusP = 1 - p;
    double bound;


    cdfbin(&which, &cumulativeProb, &oneMinusCumulativeProb, &x, &n, &p, &oneMinusP, &status,
        &bound);

    if (status != 0)
    {
        printf("Error in computing cdfbin, status = %d!\n", status);
        abort();
    }

    if (oneMinus)
    {
        return oneMinusCumulativeProb;
    }
    else
    {
        return cumulativeProb;
    }
}

int main()
{
    const double probability_lower_coin = 0.5;
    const double probability_higher_coin = 0.6;
    const int n_max = 1e5;
    for (int n = 1; n <= n_max; ++n)
    {
        printf("n = %d/%d\n", n, n_max);
        for (int c = 1; c <= n; ++c)
        {
            const double probability_for_false_positive = binomialDistributionFunction(c-1, n, probability_lower_coin, true);
            const double probability_for_false_negative = binomialDistributionFunction(c-1, n, probability_higher_coin, false);
            if (probability_for_false_negative <= 0.05 && probability_for_false_positive <= 0.05)
            {
                printf("%d coin tosses, higher coin if heads >= %d⁠⁠", n, c);
                printf("probability for false negative = %g, probability for false positive = %g⁠⁠\n", probability_for_false_negative,
                    probability_for_false_positive);
                return 0;
            }
        }
    }

    printf("failed⁠\n");
    return 0;
}
