#pragma once
#include <stdint.h>


typedef int_fast64_t Znum;
typedef uint_fast64_t Nnum;
typedef double Rnum;

#ifdef __cplusplus
   #include <complex>
   typedef std::complex<Rnum> Cnum;
#else
   #include <complex.h>
   typedef double _Complex Cnum;
#endif

