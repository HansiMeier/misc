#include <boost/units/quantity.hpp>
#include <boost/units/physical_dimensions.hpp>
#include <boost/units/systems/si.hpp>
#include <boost/units/cmath.hpp>

#ifndef UNIT_DATATYPE
#define UNIT_DATATYPE double
#endif

#ifndef UNIT_DONT_IMPORT_NAMESPACES
using namespace ::boost::units::si;
#endif

typedef ::boost::units::quantity<::boost::units::unit<::boost::units::absorbed_dose_dimension, ::boost::units::si::system>, UNIT_DATATYPE> AbsorbedDose;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::acceleration_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Acceleration;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::action_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Action;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::activity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Activity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::amount_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Amount;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::angular_acceleration_dimension, ::boost::units::si::system>, UNIT_DATATYPE> AngularAcceleration;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::angular_momentum_dimension, ::boost::units::si::system>, UNIT_DATATYPE> AngularMomentum;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::angular_velocity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> AngularVelocity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::area_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Area;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::capacitance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Capacitance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::conductance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Conductance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::conductivity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Conductivity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::current_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Current;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::dose_equivalent_dimension, ::boost::units::si::system>, UNIT_DATATYPE> DoseEquivalent;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::dynamic_viscosity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> DynamicViscosity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::electric_charge_dimension, ::boost::units::si::system>, UNIT_DATATYPE> ElectricCharge;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::electric_potential_dimension, ::boost::units::si::system>, UNIT_DATATYPE> ElectricPotential;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::energy_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Energy;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::energy_density_dimension, ::boost::units::si::system>, UNIT_DATATYPE> EnergyDensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::force_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Force;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::frequency_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Frequency;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::heat_capacity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> HeatCapacity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::illuminance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Illuminance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::impedance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Impedance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::inductance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Inductance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::kinematic_viscosity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> KinematicViscosity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::length_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Length;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::luminance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Luminance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::luminous_flux_dimension, ::boost::units::si::system>, UNIT_DATATYPE> LuminousFlux;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::luminous_intensity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> LuminousIntensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::magnetic_field_intensity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MagneticFieldIntensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::magnetic_flux_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MagneticFlux;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::magnetic_flux_density_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MagneticFluxDensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::mass_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Mass;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::mass_density_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MassDensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::molar_energy_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MolarEnergy;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::molar_heat_capacity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MolarHeatCapacity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::moment_of_inertia_dimension, ::boost::units::si::system>, UNIT_DATATYPE> MomentOfInertia;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::momentum_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Momentum;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::permeability_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Permeability;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::permittivity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Permittivity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::plane_angle_dimension, ::boost::units::si::system>, UNIT_DATATYPE> PlaneAngle;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::power_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Power;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::pressure_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Pressure;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::reluctance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Reluctance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::resistance_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Resistance;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::resistivity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Resistivity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::solid_angle_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SolidAngle;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::specific_energy_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SpecificEnergy;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::specific_heat_capacity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SpecificHeatCapacity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::specific_volume_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SpecificVolume;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::stress_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Stress;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::surface_density_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SurfaceDensity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::surface_tension_dimension, ::boost::units::si::system>, UNIT_DATATYPE> SurfaceTension;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::temperature_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Temperature;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::thermal_conductivity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> ThermalConductivity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::time_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Time;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::torque_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Torque;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::velocity_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Velocity;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::volume_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Volume;
typedef ::boost::units::quantity<::boost::units::unit<::boost::units::wavenumber_dimension, ::boost::units::si::system>, UNIT_DATATYPE> Wavenumber;
