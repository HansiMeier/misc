#!/usr/bin/env python

import ctypes as ct
import matplotlib as mpl
import platform
import cstruct as cs

do_blit = True
if platform.system() == 'Darwin':
    # The 'macosx' backend used by matplotlib as default doesn't support blit
    try:
        mpl.use('QT5Agg')
    except (KeyboardInterrupt, SystemExit):
        # Why o why is KeyboardInterrupt an ordinary exception !?
        raise
    except:
        do_blit = False
else:
    mpl.use('TkAgg')

import matplotlib.pyplot as plt
import matplotlib.animation as an
from math import *
import builtins

radius = 0.5
fig = plt.figure()
final_time = 1.0
fps = 24

def load_variables(library):
    to_define = (
            'springStiffness', 'mass', 'position',
            'timeStep', 'velocity', 'gravity'
    )

    field_descriptions = [(name, ct.c_double) for name in to_define]

    return cs.access(library, 'globals', field_descriptions)

    

def load_implementation():
    return ct.cdll.LoadLibrary("./libharmos.dylib")

def get_total_energy():
    return 0.5 * cglobals.mass * cglobals.velocity**2 \
           + 0.5 * cglobals.springStiffness * cglobals.position ** 2 \
           - cglobals.mass * cglobals.position * cglobals.gravity

def get_y_limits():
    global radius
    padding = 4*radius
    total_energy = get_total_energy()
    gravitational_force = cglobals.mass * cglobals.gravity
    # get extremal cglobals.positions by conservation of energy and
    # kinetic energy = 0 at the extremal cglobals.positions
    sqrt_of_descriminant = sqrt(gravitational_force**2 + 2*cglobals.springStiffness * total_energy)
    upper_limit = (gravitational_force + sqrt_of_descriminant) / cglobals.springStiffness + padding
    lower_limit = (gravitational_force - sqrt_of_descriminant) / cglobals.springStiffness - padding

    return (lower_limit, upper_limit)


    

def set_up_figure():
    global ax, fig, mass_point, radius, cglobals
    ax = plt.subplot(aspect='equal')
    ax.set_title('Harmonic oscillator')
    mass_point = plt.Circle((0, cglobals.position), 0.5, color='red', alpha=0.5)
    ax.set_xlim(-10*radius, 10*radius)
    ax.set_ylim(*get_y_limits())
    ax.add_patch(mass_point)
    return (mass_point,)


def update_figure(lib):
    global cglobals, mass_point
    lib.evolve()
    x = mass_point.center[0]
    mass_point.center = (x, cglobals.position)
    return (mass_point,)


def main():
    global final_time, fps, do_blit, cglobals
    lib = load_implementation()
    cglobals = load_variables(lib)
    cglobals.timeStep = -0.1
    cglobals.gravity /= 10
    cglobals.velocity = -1.0
    def animate(frame_number):
        nonlocal lib
        return update_figure( lib)

    def init():
        return set_up_figure()

    number_of_frames = int(round(final_time * fps)) # must be an int
    interval = int(round(1000.0 / fps)) # Must be a whole number of milliseconds
    anim = an.FuncAnimation(fig, animate, frames=number_of_frames, interval=interval, init_func=init, blit=do_blit, repeat=True)
    plt.show()

main()



    

