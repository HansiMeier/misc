
typedef struct Globals_Struct {
    double springStiffness;
    double mass;
    double position;
    double timeStep;
    double velocity;
    double gravity;
} Globals;

Globals globals = {
    .springStiffness = 1.0, // in N/m
    .mass = 1.0, // in kg
    .position = 0.0, // in m
    .timeStep = 1e-6, // in s
    .velocity = 0.0, // in m/s
    .gravity = -9.81 // in m/s^2
};
    


double
computeAcceleration(
) {
    return -globals.springStiffness * globals.position / globals.mass + globals.gravity;
}


void
evolve(
) {
    // using velocity verlet
    const double currentAcceleration = computeAcceleration();
    globals.position += globals.timeStep * (globals.velocity + 0.5 * globals.timeStep * currentAcceleration);
    const double newAcceleration = computeAcceleration();
    globals.velocity += globals.timeStep * 0.5 * (currentAcceleration + newAcceleration);
}


