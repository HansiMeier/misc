import ctypes

def access(
    library,
    c_struct_name,
    field_descriptions
):
    class CStruct(ctypes.Structure):
        nonlocal field_descriptions
        _fields_ = field_descriptions

    return CStruct.in_dll(library, c_struct_name)




