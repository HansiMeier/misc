#if ! defined EIGENITERATORS_HPP
   #define EIGENITERATORS_HPP 1

   #if defined EIGEN_MATRIXBASE_PLUGIN
      #error "You're trying to include another Eigen::MatrixBase plugin!"
   #endif

   #define EIGEN_MATRIXBASE_PLUGIN "EigenIterators.hpp"
   #include <iterator>
   #if defined EIGEN_MATRIXBASE_H
      #error "Please include Eigen after EigenIterators.hpp"
   #endif

   #if __cplusplus < 201103L
      #error "This code is C++11!"
   #endif
#elif EIGENITERATORS_HPP == 1 && defined EIGEN_MATRIXBASE_H
   #undef EIGENITERATORS_HPP
   #define EIGENITERATORS_HPP 2

   #pragma message "Adding iterators to MatrixBase"
   using iterator = Scalar*;
   using const_iterator = const Scalar*;
   using reverse_iterator = iterator;
   using const_reverse_iterator = const_iterator;

   iterator begin() {
      return this->derived().data();
   }
   
   iterator end() {
      return this->derived().data() + this->size();
   }

   const_iterator cbegin() const {
      return this->derived().data();
   }

   const_iterator cend() const {
      return this->derived().data() + this->size();
   }

   reverse_iterator rbegin() {
      return this->end() - 1;
   }
   
   reverse_iterator rend() {
      return this->begin() - 1;
   }

   const_reverse_iterator crbegin() const {
      return this->cend() - 1;
   }

   const_reverse_iterator crend() const {
      return this->cbegin() - 1;
   }
#endif

