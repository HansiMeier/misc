#include "intdiv.h"
#include <stdio.h>

void test(const int numerator, const int denominator, const int expectedResult)
{
    const int computedResult = roundedDiv(numerator, denominator);
    printf("roundedDiv(%d, %d) = %d", numerator, denominator, computedResult);
    if (computedResult == expectedResult)
    {
        printf(" correct\n");
    }
    else
    {
        printf(" wrong, should be %d\n", expectedResult);
    }
}

int main()
{
    test(4, 2, 2);
    test(-1, 4, 0);
    test(-8, -3, 3);
    test(11, 2, 6);
    test(0, 3, 0);
    return 0;
}