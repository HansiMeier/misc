#include "EigenScalarsAs1x1Matrices.hpp"
#include <Eigen/Dense>
#include <iostream>

int main() {
   const Eigen::MatrixXd A(1.5);

   std::cout << "A = " << A
             << "\nrows = " << A.rows()
             << "\ncols = " << A.cols()
             << "\n";
   return 0;
}
