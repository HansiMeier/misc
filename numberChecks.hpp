#pragma once
#if ! defined NDEBUG || defined NUMBER_CHECKS

#include <limits>
#include <iostream>
#include <sstream>
#include <stdexcept>


namespace NumberChecks {

    template <class FirstArg, class... Rest>
    inline
    void
    concatenate(
        std::stringstream& ss,
        FirstArg first,
        Rest... rest
    ) {
        concatenate(ss << first, rest);
    }

    template <class Arg>
    inline
    void
    concatenate(
        std::stringstream& ss,
        Arg arg
    ) {
        ss << arg;
    }

    template <class... ToOutput>
    inline
    void
    numericAssert(
        const bool condition,
        ToOutput... args
    ) {
        if (!condition) {
            std::stringstream ss;
            concatenate(ss, args...);
            throw std::domain_error(ss.str());
        }
    }



    template <double lowerBound, double upperBound, typename Number>
    class InOpenInterval {

        /**************************************************************************************************
         **************************************************************************************************
         **                                        PUBLIC SECTION                                        **
         **************************************************************************************************
         **************************************************************************************************/
        public:
            using ThisClass = InOpenInterval<lowerBound, upperBound, Number>;
            static double thresholdForZero_S(1e-20);

            /**************************************************************************************************
             *                                  CONSTRUCTORS AND ASSIGNMENTS                                  *
             **************************************************************************************************/

            InOpenInterval(
                 ThisClass other
            ): value_M(Number(std::move(other.value_M))) {
            }

            template <typename OtherNumber>
            InOpenInterval(
                OtherNumber inputValue
            ): value_M(Number(std::move(inputValue))) {
                numericAssert(inputValue > lowerBound && inputValue < upperBound,
                              inputValue, " doesn't fulfill ", lowerBound, " < ", inputValue, " < ", upperBound, "!");
            }

            // no explicit destructor

            ThisClass&
            operator=(
                ThisClass other
            ) {
                value_M = std::move(other.value_M);
            }


            template <typename OtherNumber>
            ThisClass&
            operator=(
                OtherNumber assigned
            ) {
                const ThisClass checkedAssigned(assigned);
                *this = checkedAssigned;
                return *this;
            }


            /**************************************************************************************************
             *                                          CONVERSIONS                                           *
             **************************************************************************************************/

            Number
            operator Number(
            ) const {
                return value_M;
            }

            /**************************************************************************************************
             *                                           ARITHMETIC                                           *
             **************************************************************************************************/

            // addition
            ThisClass&
            operator+=(
                ThisClass assigned
            ) {
                value_M += std::move(assigned.value_M);
                return *this; 
            }

            ThisClass
            operator+(
               ThisClass value
            ) const {
                return tmp += *this;
            }

            // subtraction
            ThisClass&
            operator-=(
                ThisClass assigned
            ) {
                value_M -= std::move(assigned.value_M);
                return *this;
            }

            ThisClass
            operator-(
               ThisClass value
            ) const {
                return value -= *this;
            }

            // multiplication
            ThisClass&
            operator*=(
                ThisClass assigned
            ) {
                value_M *= std::move(assigned.value_M);
                return *this;
            }

            ThisClass
            operator*(
               ThisClass value
            ) const {
                return value *= *this;
            }

            // division
            ThisClass&
            operator/=(
                ThisClass assigned
            ) {
                numericAssert(std::abs(assigned.value_M) < thresholdForZero_S,
                              "Error trying to divide ", value_M, " by ", assigned.value_M, "; divisor almost zero!"
                );
                value_M /= std::move(assigned.value_M);
                return *this;
            }

            ThisClass
            operator/(
               ThisClass value
            ) const {
                return value /= *this;
            }


            /**************************************************************************************************
             *                                          COMPARISONS                                           *
             **************************************************************************************************/


            template <typename OtherNumber>
            bool
            operator==(
                OtherNumber other
            ) {
                return other == value_M;
            }


            template <double OtherLowerBound, double OtherUpperBound, typename OtherNumber>
            bool
            operator==(
                const InOpenInterval<OtherLowerBound, OtherUpperBound, OtherNumber>& other
            ) {
                return other.value_M == value_M;
            }

            
            template <typename OtherNumber>
            bool
            operator!=(
                OtherNumber other
            ) {
                return other != value_M;
            }


            template <double OtherLowerBound, double OtherUpperBound, typename OtherNumber>
            bool
            operator!=(
                const InOpenInterval<OtherLowerBound, OtherUpperBound, OtherNumber>& other
            ) {
                return other.value_M != value_M;
            }

            
            template <typename OtherNumber>
            bool
            operator<=(
                OtherNumber other
            ) {
                return other <= value_M;
            }


            template <double OtherLowerBound, double OtherUpperBound, typename OtherNumber>
            bool
            operator<=(
                const InOpenInterval<OtherLowerBound, OtherUpperBound, OtherNumber>& other
            ) {
                return other.value_M <= value_M;
            }

            
            template <typename OtherNumber>
            bool
            operator>(
                OtherNumber other
            ) {
                return other > value_M;
            }


            template <double OtherLowerBound, double OtherUpperBound, typename OtherNumber>
            bool
            operator>(
                const InOpenInterval<OtherLowerBound, OtherUpperBound, OtherNumber>& other
            ) {
                return other.value_M > value_M;
            }

            
            template <typename OtherNumber>
            bool
            operator<(
                OtherNumber other
            ) {
                return other < value_M;
            }


            template <double OtherLowerBound, double OtherUpperBound, typename OtherNumber>
            bool
            operator<(
                const InOpenInterval<OtherLowerBound, OtherUpperBound, OtherNumber>& other
            ) {
                return other.value_M < value_M;
            }

        private:
            Number value_M;
    }; 
}


                

#endif
