#pragma once
#include <type_traits>

template <typename T>
class ptr {
    public:

        using RawPointer = typename std::remove_const<T>::type*;
        using ConstCorrectRawPointer = typename std::conditional<std::is_const<T>::value, const RawPointer, RawPointer>::type;
        using ThisClass = ptr<T>;


        /**************************************************************************************************
         *                                  CONSTRUCTORS AND DESTRUCTORS                                  *
         **************************************************************************************************/

        inline
        ptr(ConstCorrectRawPointer p);

        inline
        ptr();


        /**************************************************************************************************
         *                                     ARITHMETIC OPERATIONS                                      *
         **************************************************************************************************/

        inline
        ThisClass&
        operator++();

        inline
        ThisClass
        operator++(int);


        template <typename Integral>
        inline
        ThisClass
        operator+(const Integral shift);

        template <typename Integral>
        inline
        ThisClass&
        operator+=(const Integral shift);
        
        inline
        ThisClass&
        operator--();

        inline
        ThisClass
        operator--(int);


        inline
        std::ptrdiff_t
        operator-(const ThisClass otherPtr) const;

        inline
        std::ptrdiff_t
        operator-(ConstCorrectRawPointer otherPtr) const;


        template <typename Integral>
        inline
        ThisClass
        operator-(const Integral shift);

        template <typename Integral>
        inline
        ThisClass&
        operator-=(const Integral shift);


        /**************************************************************************************************
         *                                      CONVERSION OPERATORS                                      *
         **************************************************************************************************/

        inline
        operator ConstCorrectRawPointer() const;


        /**************************************************************************************************
         *                                     ASSIGNMENT OPERATIONS                                      *
         **************************************************************************************************/

        inline
        ThisClass&
        operator=(ConstCorrectRawPointer otherPtr);


        inline
        ThisClass&
        operator=(const ThisClass otherPtr);



        




        







};
