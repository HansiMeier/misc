#include <omp.h>
#include <iostream>
#include <cstdlib>


void
nestedParallelism(const int level, const int numberOfThreadsToSpawn) {
    if (level <= 0) {
        return;
    }

    #pragma omp critical
    {
        std::cout << "Level = " << level << ", id = " << omp_get_thread_num() << "\n";
    }
    #pragma omp parallel num_threads(numberOfThreadsToSpawn)
    {
        nestedParallelism(level-1, numberOfThreadsToSpawn);
    }
}


int
main() {
    omp_set_dynamic(0);
    nestedParallelism(3, 2);

    return EXIT_SUCCESS;
}



