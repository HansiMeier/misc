#pragma once
#include <Eigen/Core>
#include "simulation.hpp"

class Particle {
    public:
        using Scalar = Simulation::Scalar;
        using Vector3D = Simulation::Vector3D;

        Vector3D& position;
        Vector3D& velocity;
        Vector3D& force;
        Scalar& mass;

        Scalar
        kineticEnergy(
        );

        Scalar
        potentialEnergy(
        );

        Scalar
        totalEnergy(
        );
};


