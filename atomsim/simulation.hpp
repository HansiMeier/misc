#pragma once
#include <vector>
#include <Eigen/Dense>

class Simulation {
    public:
        using Scalar = double;
        using Vector3D = Eigen::Matrix<Scalar, 3, 1>;
        using Vector3ND = std::vector<Vector3D>;
        using VectorND = std::vector<Scalar>;

        Simulation(
            Vector3ND positions,
            Vector3ND velocities,
            VectorND masses,
            Vector3D boxDimensions,
            Scalar timestep
        );

        void
        doTimestep(
        );

        Scalar
        totalKineticEnergy(
        ) const;

        Scalar
        totalPotentialEnergy(
        ) const;

        Scalar
        totalEnergy(
        ) const;

        Scalar
        virial(
        ) const;

    private:
        Vector3ND positions;
        Vector3ND velocities;
        VectorND masses;
        const Vector3D boxDimensions;
        const Scalar timestep;

        void
        calculateForces(
        );

        void
        integrateTimestep(
        );

        void
        putParticleInsideBox(
            Vector3D& position
        );

        void
        assurePeriodicBoundaryConditions(
        );
};



