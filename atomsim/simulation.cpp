#include "simulation.hpp"
using Scalar = Simulation::Scalar;
using Vector3D = Simulation::Vector3D;
using Vector3ND = Simulation::Vector3ND;
using VectorND = Simulation::VectorND;


/**************************************************************************************************
 *                                  PERIODIC BOUNDARY CONDITIONS                                  *
 **************************************************************************************************/

static
Scalar
unsafeModulo(
    Scalar moduland,
    const Scalar modulo
) {
    moduland += modulo * ((moduland < 0) - (moduland >= modulo));
    return moduland;
}


void
Simulation::putParticleInsideBox(
    Vector3D& particle
) {
    Scalar* entry(particle.data());
    const Scalar* end(entry + 3);
    const Scalar* dimension(boxDimensions.data());
    for (; entry != end; ++entry, ++dimension) {
        *entry = unsafeModulo(*entry, *dimension);
    }
}

void
Simulation::assurePeriodicBoundaryConditions(
) {
    for (Vector3D& position: positions) {
        putParticleInsideBox(particle);
    }
}
