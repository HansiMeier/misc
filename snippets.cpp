// Better assert (may need C++11)
// to be used instead of cassert

#include <cstdlib>
#include <iostream>
#ifndef NDEBUG
   #define assert(cond) \
      if (!(cond)) { \
         std::cerr << "Assert \"" << #cond << "\" failed in file \"" \
                   << __FILE__ << "\", line " << __LINE__ \
                   << ", function \"" << __func__ << "\"!" \
                   << std::endl; \
         std::abort(); \
      }
#else
   #define assert(cond)
#endif

