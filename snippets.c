
// Require a C99 compliant compiler
#if __STDC_VERSION__ < 199901L
   #error "Requires C99-compliant compiler!"
#endif

// Better assert (C99)
// to be used instead of assert.h
#include <stdio.h>
#include <stdlib.h>
#ifndef NDEBUG
   #define assert(cond) \
      if (!(cond)) { \
         fprintf( \
            stderr, \
            "Assert \"%s\" failed in file \"%s\", line %d, function \"%s\"!\n", \
            #cond, \
            __FILE__, \
            __LINE__, \
            __func__ \
         ); \
         fflush(stderr); \
         abort(); \
      }
#else
   #define assert(cond)
#endif


   
