#ifndef BPRINTER_TABLE_PRINTER_H_
#define BPRINTER_TABLE_PRINTER_H_

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <stdexcept>

namespace bprinter {
class endl {};
/** \class TablePrinter

  Print a pretty table into your output of choice.

  Usage:
    TablePrinter tp(std::cout);
    tp.addColumn("Name", 25);
    tp.addColumn("Age", 3);
    tp.addColumn("Position", 30);

    tp.printHeader();
    tp << "Dat Chu" << 25 << "Research Assistant";
    tp << "John Doe" << 26 << "Professional Anonymity";
    tp << "Jane Doe" << tp.SkipToNextLine();
    tp << "Tom Doe" << 7 << "Student";
    tp.printFooter();

  \todo Add support for padding in each table cell
  */
class TablePrinter {
public:
    TablePrinter(std::ostream& output, const std::string& separator = "|");
    ~TablePrinter();

    inline int getNumberOfColumns() const;
    inline int getTableWidth() const;
    inline void set_separator(const std::string& separator);
    inline void setFlushLeft();
    inline void setFlushRight();

    inline void addColumn(const std::string& headerName, int columnWidth);
    inline void printHeader();
    inline void printFooter();

    inline TablePrinter& operator<<(endl input) {
        while (indexOfCurrentColumn_M != 0) {
            *this << "";
        }
        return *this;
    }

    // Can we merge these?
    inline TablePrinter& operator<<(float input);
    inline TablePrinter& operator<<(double input);

    template<typename T> TablePrinter& operator<<(T input) {
        if (indexOfCurrentColumn_M == 0) {
            outStream_M << "|";
        }

        if (flushLeft_M) {
            outStream_M << std::left;
        } else {
            outStream_M << std::right;
        }

        // Leave 3 extra space: One for negative sign, one for zero, one for decimal
        outStream_M << std::setw(columnWidths_M.at(indexOfCurrentColumn_M))
                     << input;

        if (indexOfCurrentColumn_M == getNumberOfColumns() - 1) {
            outStream_M << "|\n";
            indexOfCurrentRow_M = indexOfCurrentRow_M + 1;
            indexOfCurrentColumn_M = 0;
        } else {
            outStream_M << separator_M;
            indexOfCurrentColumn_M = indexOfCurrentColumn_M + 1;
        }

        return *this;
    }

private:
    inline void printHorizontalLine();

    template<typename T> void outputDecimalNumber(T input);

    std::ostream& outStream_M;
    std::vector<std::string> columnHeaders_M;
    std::vector<int> columnWidths_M;
    std::string separator_M;

    int indexOfCurrentRow_M; // index of current row
    int indexOfCurrentColumn_M; // index of current column

    int tableWidth_M;
    bool flushLeft_M;
};

}



namespace bprinter {

template<typename T> void TablePrinter::outputDecimalNumber(T input) {
    // If we cannot handle this number, indicate so
    if (input < 10 * (columnWidths_M.at(indexOfCurrentColumn_M) - 1) || input > 10 * columnWidths_M.at(indexOfCurrentColumn_M)) {
        std::stringstream string_out;
        string_out << std::setiosflags(std::ios::fixed)
                   << std::setprecision(columnWidths_M.at(indexOfCurrentColumn_M))
                   << std::setw(columnWidths_M.at(indexOfCurrentColumn_M))
                   << input;

        std::string stringRepresentationOfNumber = string_out.str();

        stringRepresentationOfNumber[columnWidths_M.at(indexOfCurrentColumn_M) - 1] = '*';
        std::string stringToPrint = stringRepresentationOfNumber.substr(0, columnWidths_M.at(indexOfCurrentColumn_M));
        outStream_M << stringToPrint;
    } else {

        // determine what precision we need
        int precision = columnWidths_M.at(indexOfCurrentColumn_M) - 1; // leave room for the decimal point
        if (input < 0) {
            --precision;    // leave room for the minus sign
        }

        // leave room for digits before the decimal?
        if (input < -1 || input > 1) {
            int numberOfDigitsBeforeDecimalPoint = 1 + (int)log10(std::abs(input));
            precision -= numberOfDigitsBeforeDecimalPoint;
        } else {
            precision --;    // e.g. 0.12345 or -0.1234
        }

        if (precision < 0) {
            precision = 0;    // don't go negative with precision
        }

        outStream_M << std::setiosflags(std::ios::fixed)
                     << std::setprecision(precision)
                     << std::setw(columnWidths_M.at(indexOfCurrentColumn_M))
                     << input;
    }

    if (indexOfCurrentColumn_M == getNumberOfColumns() - 1) {
        outStream_M << "|\n";
        indexOfCurrentRow_M = indexOfCurrentRow_M + 1;
        indexOfCurrentColumn_M = 0;
    } else {
        outStream_M << separator_M;
        indexOfCurrentColumn_M = indexOfCurrentColumn_M + 1;
    }
}
}


namespace bprinter {
TablePrinter::TablePrinter(std::ostream& output, const std::string& separator):
    outStream_M(output),
    separator_M(separator),
    indexOfCurrentRow_M(0),
    indexOfCurrentColumn_M(0),
    tableWidth_M(0),
    flushLeft_M(false) {
}

TablePrinter::~TablePrinter() {

}

int TablePrinter::getNumberOfColumns() const {
    return columnHeaders_M.size();
}

int TablePrinter::getTableWidth() const {
    return tableWidth_M;
}

void TablePrinter::set_separator(const std::string& separator) {
    separator_M = separator;
}

void TablePrinter::setFlushLeft() {
    flushLeft_M = true;
}

void TablePrinter::setFlushRight() {
    flushLeft_M = false;
}

/** \brief Add a column to our table
 **
 ** \param headerName Name to be print for the header
 ** \param columnWidth the width of the column (has to be >=5)
 ** */
void TablePrinter::addColumn(const std::string& headerName, int columnWidth) {
    if (columnWidth < 4) {
        throw std::invalid_argument("Column size has to be >= 4");
    }

    columnHeaders_M.push_back(headerName);
    columnWidths_M.push_back(columnWidth);
    tableWidth_M += columnWidth + separator_M.size(); // for the separator
}

void TablePrinter::printHorizontalLine() {
    outStream_M << "+"; // the left bar

    for (int i = 0; i < tableWidth_M - 1; ++i) {
        outStream_M << "-";
    }

    outStream_M << "+"; // the right bar
    outStream_M << "\n";
}

void TablePrinter::printHeader() {
    printHorizontalLine();
    outStream_M << "|";

    for (int i = 0; i < getNumberOfColumns(); ++i) {

        if (flushLeft_M) {
            outStream_M << std::left;
        } else {
            outStream_M << std::right;
        }

        outStream_M << std::setw(columnWidths_M.at(i)) << columnHeaders_M.at(i).substr(0, columnWidths_M.at(i));
        if (i != getNumberOfColumns() - 1) {
            outStream_M << separator_M;
        }
    }

    outStream_M << "|\n";
    printHorizontalLine();
}

void TablePrinter::printFooter() {
    printHorizontalLine();
}

TablePrinter& TablePrinter::operator<<(float input) {
    outputDecimalNumber<float>(input);
    return *this;
}

TablePrinter& TablePrinter::operator<<(double input) {
    outputDecimalNumber<double>(input);
    return *this;
}

}
#endif
