#ifndef myAssert_hpp
#define myAssert_hpp

#if __cplusplus < 201103L
   #error "myAssert.hpp needs at least C++11!"
#endif

#ifndef NDEBUG

   #include <cstdlib>
   #include <iostream>
   #include <boost/preprocessor/seq/for_each.hpp>
   #include <boost/preprocessor/variadic/to_seq.hpp>
   #include <boost/preprocessor/stringize.hpp>

   #define myAssertImplementation__(r, name, elem)  \
      if (!(elem)) { \
         std::cerr << "\n" << (name) << " FAILED\n--------------------------------------------------------" \
                   << "\nfile: " << __FILE__ \
                   << "\nfunction: " << __func__ \
                   << "\nline: " << __LINE__ \
                   << "\ncondition which failed: " << BOOST_PP_STRINGIZE(elem) \
                   << "\n"; \
         std::cerr.flush(); \
         std::exit(23); \
      }
   // the do {...} while(false) is here to enable the use of this macro inside a block
   // see http://www.bruceblinn.com/linuxinfo/DoWhile.html
   #define myAssertCheck__(name, seq) \
      do { \
         BOOST_PP_SEQ_FOR_EACH(myAssertImplementation__, name, seq); \
      } while(false)

#else
   #define myAssertCheck__(name, seq)
#endif

#define invar(...) myAssertCheck__("INVARIANT", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define precond(...) myAssertCheck__("PRECONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define postcond(...) myAssertCheck__("POSTCONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
#define require(...) precond(__VA_ARGS__)
#define ensure(...) postcond(__VA_ARGS__)

#ifdef assert
   #undef assert
#endif
#define assert(...) myAssertCheck__("ASSERTION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

#endif
