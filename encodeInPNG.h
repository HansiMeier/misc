#ifndef ENCODE_IN_PNG_H
#define ENCODE_IN_PNG_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <png.h>

// Require a C99 compliant compiler
#if __STDC_VERSION__ < 199901L
   #error "Requires C99-compliant compiler!"
#endif


typedef enum {
   SUCCESS,
   FINISHED,
   CLEANED_UP,
   ERR_OPEN,
   ERR_SIGNATURE,
   ERR_PNGSTRUCT,
   ERR_IO,
   ERR_JMP,
   ERR_INTERLACED,
   ERR_ALLOC
} encodeInPNG_error_t;


/**************************************************************************************************
 *                                            PNG I/O                                             *
 **************************************************************************************************/

encodeInPNG_error_t readDataAsPNG(
      const char* filename,
      uintmax_t* sizep,
      char** bufferp,
      bool rowwise
);

bool calcWidthHeight(
      uintmax_t pixels,
      png_uint_32p widthp,
      png_uint_32p heightp
);

void choosePNGSettings(
      uintmax_t* pixelsp,
      png_bytep colourTypep,
      png_bytep bitDepthp
);

void setCompressionLevel(png_structp png_ptr, const uintmax_t size);

encodeInPNG_error_t writePNG(
      const char* filename,
      const size_t filenameLength,
      const uintmax_t size,
      const char* buffer
);

#include "encodeInPNG.c"
#endif
