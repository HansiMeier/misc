#pragma once
#include <atomic>
#include <mutex>

class FifoLock {
    public:
        FifoLock(
        );

        void
        unlock(
        );

        void
        lock(
        );

    private:
        std::atomic_int numberToAssign_M;
        std::atomic_int numberToServe_M;

        std::unique_lock<std::mutex> turnLock_M;
        std::condition_variable forMyTurn_M;

};


