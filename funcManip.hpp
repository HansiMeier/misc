#pragma once
#include <tuple>
#include <type_traits>
#include <cstddef>
namespace funcManip {

   template <typename Return, typename... Arguments>
   struct function_traits<Return(Arguments...)> {
      using return_type = Return;

      static constexpr std::size_t arity(sizeof...(Arguments));

      template <std::size_t pos>
      struct arg {
         static_assert(pos < arity);
         using type = typename std::tuple_element<pos, std::tuple<Arguments...>>::type;
      };

      // some convenience usings
      using argument_types = std::tuple<Arguments...>;
      using first_type = typename arg<0>::type;
      using second_type = typename arg<1>::type;
      using third_type = typename arg<2>::type;
   };


   // Redirect function pointer to function object
   template <typename Return, typename... Arguments>
   struct function_traits<Return(*)(Arguments...)>: public function_traits<Return(Arguments...)> {
   };

   template <std::size_t n, typename Function>
   constexpr bool
   hasArity(
      Function f
   ) {
      return (function_traits<Function>::arity == n);
   }

   template <std::size_t n, typename Function>
   constexpr void
   requireArity(
      Function f
   ) {
      static_assert(hasArity<n>(f));
   }

   template <typename Function>
   decltype(auto)
   flip(
      const Function& f
   ) {
      requireArity(2);

      using First = typename function_traits<Function>::first_type;
      using Second = typename function_traits<Function>::second_type;
      using Return = typename function_traits<Function>::return_type;

      return [] (Second b, First a) -> Return {
         return f(a, b);
      };
   };

   template <typename FunctionF, typename FunctionG, typename... FunctionGArgs>
   decltype(auto)
   compose(
      const FunctionF& f,
      const FunctionG& g
   ) {
      static_assert(
         std::is_convertible<
            typename function_traits<FunctionG>::return_type,
            typename function_traits<FunctionF>::first_type
         >::value
      );

      return [&f, &g] (FunctionGArgs... args) {
         return f(g(args...));
      };
   }


   template <typename FunctionF, typename... OtherFunctions>
   decltype(auto)
   compose(
      const FunctionF& f,
      OtherFunctions... funcs
   ) {
      return compose(f, compose(funcs...));
   }
}

