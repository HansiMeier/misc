#pragma once

#include <Eigen/Core>
#include <Eigen/SparseCore>

#ifndef EIGEN_TYPES_DIM
   #define EIGEN_TYPES_DIM Eigen::Dynamic
#endif

#ifndef EIGEN_TYPES_SCALAR
   #define EIGEN_TYPES_SCALAR double
#endif

#ifndef EIGEN_TYPES_INDEX
   #define EIGEN_TYPES_INDEX int
#endif

#ifndef EIGEN_TYPES_STORAGEORDER
   #define EIGEN_TYPES_STORAGEORDER Eigen::ColMajor
#endif

typedef EIGEN_TYPES_SCALAR Scalar;
typedef EIGEN_TYPES_INDEX Index;

typedef Eigen::Matrix<Scalar, EIGEN_TYPES_DIM, 1, EIGEN_TYPES_STORAGEORDER> Vector;
typedef Eigen::Ref<Vector> VectorRef;
typedef const Eigen::Ref<const Vector>& ConstVectorRef;

typedef Eigen::Matrix<Scalar, EIGEN_TYPES_DIM, EIGEN_TYPES_DIM, EIGEN_TYPES_STORAGEORDER> Matrix;
typedef const Eigen::Ref<const Matrix>& ConstMatrixRef;
typedef Eigen::Ref<Matrix> MatrixRef;

typedef Eigen::SparseMatrix<Scalar, EIGEN_TYPES_STORAGEORDER, Index> SparseMatrix;
typedef const Eigen::Ref<const SparseMatrix>& ConstSparseMatrixRef;
typedef Eigen::Ref<SparseMatrix> SparseMatrixRef;


#undef EIGEN_TYPES_DIM
#undef EIGEN_TYPES_SCALAR
#undef EIGEN_TYPES_STORAGEORDER
#undef EIGEN_TYPES_INDEX

