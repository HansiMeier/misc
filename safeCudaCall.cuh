#pragma once
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


static inline
const char*
mycudaGetErrorExplanation(
    const cudaError_t errorCode
) {
    switch (errorCode) {
        case cudaSuccess: {return "cudaSuccess: The API call returned with no errors.\nIn the case of query calls, this can also mean that the operation being queried is complete (see cudaEventQuery() and cudaStreamQuery()).";}
        case cudaErrorMissingConfiguration: {return "cudaErrorMissingConfiguration: The device function being invoked (usually via cudaLaunchKernel()) was not previously configured via the cudaConfigureCall() function.";}
        case cudaErrorMemoryAllocation: {return "cudaErrorMemoryAllocation: The API call failed because it was unable to allocate enough memory to perform the requested operation.";}
        case cudaErrorInitializationError: {return "cudaErrorInitializationError: The API call failed because the CUDA driver and runtime could not be initialized.";}
        case cudaErrorLaunchFailure: {return "cudaErrorLaunchFailure: An exception occurred on the device while executing a kernel.\nCommon causes include dereferencing an invalid device pointer and accessing out of bounds shared memory.\nThe device cannot be used until cudaThreadExit() is called.\nAll existing device memory allocations are invalid and must be reconstructed if the program is to continue using CUDA.";}
        case cudaErrorLaunchTimeout: {return "cudaErrorLaunchTimeout: This indicates that the device kernel took too long to execute.\nThis can only occur if timeouts are enabled - see the device property kernelExecTimeoutEnabled for more information.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorLaunchOutOfResources: {return "cudaErrorLaunchOutOfResources: This indicates that a launch did not occur because it did not have appropriate resources.\nAlthough this error is similar to cudaErrorInvalidConfiguration, this error usually indicates that the user has attempted to pass too many arguments to the device kernel, or the kernel launch specifies too many threads for the kernel's register count.";}
        case cudaErrorInvalidDeviceFunction: {return "cudaErrorInvalidDeviceFunction: The requested device function does not exist or is not compiled for the proper device architecture.";}
        case cudaErrorInvalidConfiguration: {return "cudaErrorInvalidConfiguration: This indicates that a kernel launch is requesting resources that can never be satisfied by the current device.\nRequesting more shared memory per block than the device supports will trigger this error, as will requesting too many threads or blocks.\nSee cudaDeviceProp for more device limitations.";}
        case cudaErrorInvalidDevice: {return "cudaErrorInvalidDevice: This indicates that the device ordinal supplied by the user does not correspond to a valid CUDA device.";}
        case cudaErrorInvalidValue: {return "cudaErrorInvalidValue: This indicates that one or more of the parameters passed to the API call is not within an acceptable range of values.";}
        case cudaErrorInvalidPitchValue: {return "cudaErrorInvalidPitchValue: This indicates that one or more of the pitch-related parameters passed to the API call is not within the acceptable range for pitch.";}
        case cudaErrorInvalidSymbol: {return "cudaErrorInvalidSymbol: This indicates that the symbol name/identifier passed to the API call is not a valid name or identifier.";}
        case cudaErrorMapBufferObjectFailed: {return "cudaErrorMapBufferObjectFailed: This indicates that the buffer object could not be mapped.";}
        case cudaErrorUnmapBufferObjectFailed: {return "cudaErrorUnmapBufferObjectFailed: This indicates that the buffer object could not be unmapped.";}
        case cudaErrorInvalidHostPointer: {return "cudaErrorInvalidHostPointer: This indicates that at least one host pointer passed to the API call is not a valid host pointer.";}
        case cudaErrorInvalidDevicePointer: {return "cudaErrorInvalidDevicePointer: This indicates that at least one device pointer passed to the API call is not a valid device pointer.";}
        case cudaErrorInvalidTexture: {return "cudaErrorInvalidTexture: This indicates that the texture passed to the API call is not a valid texture.";}
        case cudaErrorInvalidTextureBinding: {return "cudaErrorInvalidTextureBinding: This indicates that the texture binding is not valid.\nThis occurs if you call cudaGetTextureAlignmentOffset() with an unbound texture.";}
        case cudaErrorInvalidChannelDescriptor: {return "cudaErrorInvalidChannelDescriptor: This indicates that the channel descriptor passed to the API call is not valid.\nThis occurs if the format is not one of the formats specified by cudaChannelFormatKind, or if one of the dimensions is invalid.";}
        case cudaErrorInvalidMemcpyDirection: {return "cudaErrorInvalidMemcpyDirection: This indicates that the direction of the memcpy passed to the API call is not one of the types specified by cudaMemcpyKind.";}
        case cudaErrorInvalidFilterSetting: {return "cudaErrorInvalidFilterSetting: This indicates that a non-float texture was being accessed with linear filtering.\nThis is not supported by CUDA.";}
        case cudaErrorInvalidNormSetting: {return "cudaErrorInvalidNormSetting: This indicates that an attempt was made to read a non-float texture as a normalized float.\nThis is not supported by CUDA.";}
        case cudaErrorInvalidResourceHandle: {return "cudaErrorInvalidResourceHandle: This indicates that a resource handle passed to the API call was not valid.\nResource handles are opaque types like cudaStream_t and cudaEvent_t.";}
        case cudaErrorNotReady: {return "cudaErrorNotReady: This indicates that asynchronous operations issued previously have not completed yet.\nThis result is not actually an error, but must be indicated differently than cudaSuccess (which indicates completion).\nCalls that may return this value include cudaEventQuery() and cudaStreamQuery().";}
        case cudaErrorInsufficientDriver: {return "cudaErrorInsufficientDriver: This indicates that the installed NVIDIA CUDA driver is older than the CUDA runtime library.\nThis is not a supported configuration.\nUsers should install an updated NVIDIA display driver to allow the application to run.";}
        case cudaErrorSetOnActiveProcess: {return "cudaErrorSetOnActiveProcess: This indicates that the user has called cudaSetValidDevices(), cudaSetDeviceFlags(), cudaD3D9SetDirect3DDevice(), cudaD3D10SetDirect3DDevice, cudaD3D11SetDirect3DDevice(), or cudaVDPAUSetVDPAUDevice() after initializing the CUDA runtime by calling non-device management operations (allocating memory and launching kernels are examples of non-device management operations).\nThis error can also be returned if using runtime/driver interoperability and there is an existing CUcontext active on the host thread.";}
        case cudaErrorInvalidSurface: {return "cudaErrorInvalidSurface: This indicates that the surface passed to the API call is not a valid surface.";}
        case cudaErrorNoDevice: {return "cudaErrorNoDevice: This indicates that no CUDA-capable devices were detected by the installed CUDA driver.";}
        case cudaErrorECCUncorrectable: {return "cudaErrorECCUncorrectable: This indicates that an uncorrectable ECC error was detected during execution.";}
        case cudaErrorSharedObjectSymbolNotFound: {return "cudaErrorSharedObjectSymbolNotFound: This indicates that a link to a shared object failed to resolve.";}
        case cudaErrorSharedObjectInitFailed: {return "cudaErrorSharedObjectInitFailed: This indicates that initialization of a shared object failed.";}
        case cudaErrorUnsupportedLimit: {return "cudaErrorUnsupportedLimit: This indicates that the cudaLimit passed to the API call is not supported by the active device.";}
        case cudaErrorDuplicateVariableName: {return "cudaErrorDuplicateVariableName: This indicates that multiple global or constant variables (across separate CUDA source files in the application) share the same string name.";}
        case cudaErrorDuplicateTextureName: {return "cudaErrorDuplicateTextureName: This indicates that multiple textures (across separate CUDA source files in the application) share the same string name.";}
        case cudaErrorDuplicateSurfaceName: {return "cudaErrorDuplicateSurfaceName: This indicates that multiple surfaces (across separate CUDA source files in the application) share the same string name.";}
        case cudaErrorDevicesUnavailable: {return "cudaErrorDevicesUnavailable: This indicates that all CUDA devices are busy or unavailable at the current time.\nDevices are often busy/unavailable due to use of cudaComputeModeExclusive, cudaComputeModeProhibited or when long running CUDA kernels have filled up the GPU and are blocking new work from starting.\nThey can also be unavailable due to memory constraints on a device that already has active CUDA work being performed.";}
        case cudaErrorInvalidKernelImage: {return "cudaErrorInvalidKernelImage: This indicates that the device kernel image is invalid.";}
        case cudaErrorNoKernelImageForDevice: {return "cudaErrorNoKernelImageForDevice: This indicates that there is no kernel image available that is suitable for the device.\nThis can occur when a user specifies code generation options for a particular CUDA source file that do not include the corresponding device configuration.";}
        case cudaErrorIncompatibleDriverContext: {return "cudaErrorIncompatibleDriverContext: This indicates that the current context is not compatible with this the CUDA Runtime.\nThis can only occur if you are using CUDA Runtime/Driver interoperability and have created an existing Driver context using the driver API.\nThe Driver context may be incompatible either because the Driver context was created using an older version of the API, because the Runtime API call expects a primary driver context and the Driver context is not primary, or because the Driver context has been destroyed.\nPlease see Interactions with the CUDA Driver API for more information.";}
        case cudaErrorPeerAccessAlreadyEnabled: {return "cudaErrorPeerAccessAlreadyEnabled: This error indicates that a call to cudaDeviceEnablePeerAccess() is trying to re-enable peer addressing on from a context which has already had peer addressing enabled.";}
        case cudaErrorPeerAccessNotEnabled: {return "cudaErrorPeerAccessNotEnabled: This error indicates that cudaDeviceDisablePeerAccess() is trying to disable peer addressing which has not been enabled yet via cudaDeviceEnablePeerAccess().";}
        case cudaErrorDeviceAlreadyInUse: {return "cudaErrorDeviceAlreadyInUse: This indicates that a call tried to access an exclusive-thread device that is already in use by a different thread.";}
        case cudaErrorProfilerDisabled: {return "cudaErrorProfilerDisabled: This indicates profiler is not initialized for this run.\nThis can happen when the application is running with external profiling tools like visual profiler.";}
        case cudaErrorAssert: {return "cudaErrorAssert: An assert triggered in device code during kernel execution.\nThe device cannot be used again until cudaThreadExit() is called.\nAll existing allocations are invalid and must be reconstructed if the program is to continue using CUDA.";}
        case cudaErrorTooManyPeers: {return "cudaErrorTooManyPeers: This error indicates that the hardware resources required to enable peer access have been exhausted for one or more of the devices passed to cudaEnablePeerAccess().";}
        case cudaErrorHostMemoryAlreadyRegistered: {return "cudaErrorHostMemoryAlreadyRegistered: This error indicates that the memory range passed to cudaHostRegister() has already been registered.";}
        case cudaErrorHostMemoryNotRegistered: {return "cudaErrorHostMemoryNotRegistered: This error indicates that the pointer passed to cudaHostUnregister() does not correspond to any currently registered memory region.";}
        case cudaErrorOperatingSystem: {return "cudaErrorOperatingSystem: This error indicates that an OS call failed.";}
        case cudaErrorPeerAccessUnsupported: {return "cudaErrorPeerAccessUnsupported: This error indicates that P2P access is not supported across the given devices.";}
        case cudaErrorLaunchMaxDepthExceeded: {return "cudaErrorLaunchMaxDepthExceeded: This error indicates that a device runtime grid launch did not occur because the depth of the child grid would exceed the maximum supported number of nested grid launches.";}
        case cudaErrorLaunchFileScopedTex: {return "cudaErrorLaunchFileScopedTex: This error indicates that a grid launch did not occur because the kernel uses file-scoped textures which are unsupported by the device runtime.\nKernels launched via the device runtime only support textures created with the Texture Object API's.";}
        case cudaErrorLaunchFileScopedSurf: {return "cudaErrorLaunchFileScopedSurf: This error indicates that a grid launch did not occur because the kernel uses file-scoped surfaces which are unsupported by the device runtime.\nKernels launched via the device runtime only support surfaces created with the Surface Object API's.";}
        case cudaErrorSyncDepthExceeded: {return "cudaErrorSyncDepthExceeded: This error indicates that a call to cudaDeviceSynchronize made from the device runtime failed because the call was made at grid depth greater than than either the default (2 levels of grids) or user specified device limit cudaLimitDevRuntimeSyncDepth.\nTo be able to synchronize on launched grids at a greater depth successfully, the maximum nested depth at which cudaDeviceSynchronize will be called must be specified with the cudaLimitDevRuntimeSyncDepth limit to the cudaDeviceSetLimit api before the host-side launch of a kernel using the device runtime.\nKeep in mind that additional levels of sync depth require the runtime to reserve large amounts of device memory that cannot be used for user allocations.";}
        case cudaErrorLaunchPendingCountExceeded: {return "cudaErrorLaunchPendingCountExceeded: This error indicates that a device runtime grid launch failed because the launch would exceed the limit cudaLimitDevRuntimePendingLaunchCount.\nFor this launch to proceed successfully, cudaDeviceSetLimit must be called to set the cudaLimitDevRuntimePendingLaunchCount to be higher than the upper bound of outstanding launches that can be issued to the device runtime.\nKeep in mind that raising the limit of pending device runtime launches will require the runtime to reserve device memory that cannot be used for user allocations.";}
        case cudaErrorNotPermitted: {return "cudaErrorNotPermitted: This error indicates the attempted operation is not permitted.";}
        case cudaErrorNotSupported: {return "cudaErrorNotSupported: This error indicates the attempted operation is not supported on the current system or device.";}
        case cudaErrorHardwareStackError: {return "cudaErrorHardwareStackError: Device encountered an error in the call stack during kernel execution, possibly due to stack corruption or exceeding the stack size limit.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorIllegalInstruction: {return "cudaErrorIllegalInstruction: The device encountered an illegal instruction during kernel execution This leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorMisalignedAddress: {return "cudaErrorMisalignedAddress: The device encountered a load or store instruction on a memory address which is not aligned.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorInvalidAddressSpace: {return "cudaErrorInvalidAddressSpace: While executing a kernel, the device encountered an instruction which can only operate on memory locations in certain address spaces (global, shared, or local), but was supplied a memory address not belonging to an allowed address space.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorInvalidPc: {return "cudaErrorInvalidPc: The device encountered an invalid program counter.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorIllegalAddress: {return "cudaErrorIllegalAddress: The device encountered a load or store instruction on an invalid memory address.\nThis leaves the process in an inconsistent state and any further CUDA work will return the same error.\nTo continue using CUDA, the process must be terminated and relaunched.";}
        case cudaErrorInvalidPtx: {return "cudaErrorInvalidPtx: A PTX compilation failed.\nThe runtime may fall back to compiling PTX if an application does not contain a suitable binary for the current device.";}
        case cudaErrorInvalidGraphicsContext: {return "cudaErrorInvalidGraphicsContext: This indicates an error with the OpenGL or DirectX context.";}
        #if __CUDACC_VER_MAJOR__ >= 8
        case cudaErrorNvlinkUncorrectable: {return "cudaErrorNvlinkUncorrectable: This indicates that an uncorrectable NVLink error was detected during the execution.";}
        #endif
        case cudaErrorStartupFailure: {return "cudaErrorStartupFailure: This indicates an internal startup failure in the CUDA runtime.";}
        default: {return "Error not known by mycudaGetErrorExplanation";};
    }
}



static inline
void
mycudaCrashOnError(
    const cudaError_t errorCode,
    const char* fileName,
    const char* functionName,
    const int line
) {
    if (errorCode == cudaSuccess) {
        return;
    }

    fprintf(stderr, "CUDA error in file %s in function %s on line %d: %s!\n%s\n", fileName, functionName, line, cudaGetErrorString(errorCode), mycudaGetErrorExplanation(errorCode));
    abort();
}



#define SAFE_CUDA_CALL(functionStem, ...) mycudaCrashOnError(cuda ## functionStem(__VA_ARGS__), __FILE__, __func__, __LINE__)

