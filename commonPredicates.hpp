#include <iterator>
#include <algorithm>
#include "funcManip.hpp"

namespace predicate {

   template <typename FwdIt, typename Comparison>
   bool isSorted(FwdIt begin, FwdIt end, bool descending = false, Comparison cmp = operator<) {
      using Value = std::iterator_traits<FwdIt>::value_type;
      if (descending) {
         Comparison cmpOld(cmp);
         cmp = [] (const Value& a, const Value& b) -> bool 

