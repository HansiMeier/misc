#include "simpleTiming.hpp"
#include <iostream>
#include <cstdlib>


using namespace simpleTiming;


int justSomeLoop() {
    int somethingRandom = 0;
    const int seed = 3;
    const int loopIterations = 100000;
    srand(seed);
    for (int i = 0; i < loopIterations; ++i) {
        somethingRandom = rand();
    }
    return somethingRandom;
}


void printSomeStars(unsigned numberOfStars) {
    std::cout << "Printing " << numberOfStars << " stars\n";
    for (unsigned star = 0; star < numberOfStars; ++star) {
        std::cout << "*";
    }
    std::cout << "\n";
}

int main() {
    double durationInSeconds;
    const int returnValueOfJustSomeLoop = timeCall(durationInSeconds, justSomeLoop);
    const int expectedReturnValueOfJustSomeLoop = 140495082;

    const char* justSomeLoopTestResult = expectedReturnValueOfJustSomeLoop == returnValueOfJustSomeLoop? " as it should.\n": " which it shouldn't!\n";
    std::cout << "justSomeLoop took " << durationInSeconds << " seconds and returned " << returnValueOfJustSomeLoop << justSomeLoopTestResult;





    const unsigned howManyStars = 1000;
    timeCall(durationInSeconds, printSomeStars, howManyStars);
    std::cout << "printSomeStars took " << durationInSeconds << " seconds.\n";


    const int numberOfRuns = 4;
    double medianDurationInSeconds;
    medianTimeCall(medianDurationInSeconds, numberOfRuns, printSomeStars, howManyStars);

    std::cout << "Median durationInSeconds of " << numberOfRuns << " calls to printSomeStars was " << medianDurationInSeconds << " seconds.\n";
    
    return 0;
}


