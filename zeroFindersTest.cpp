#include <iostream>
#include "zeroFinders.hpp"
#include <cmath>
#include <Eigen/Dense>

using namespace zeroFinders;

Vector testFunction(ConstVectorRef x) {
   return (x.array().exp() - 1).matrix();
}


decltype(auto) testFunctionDerivative(ConstVectorRef x) {
   return ([x] (ConstVectorRef b) -> Vector {
      return b.cwiseQuotient(x.array().exp().matrix());
   });
}

template <Method m>
void test() {
   const double absTol(1e-8);

   const Eigen::Vector3d a(Eigen::Vector3d::Zero());
   const Eigen::Vector3d b(Eigen::Vector3d::Constant(2));

   std::cout << fzero<Method::bisection>(a, b, testFunction, absTol)
             << "\n";

   return;
}


template <>
void test<Method::regulaFalsi>() {
   const double absTol(1e-8);

   const Eigen::Vector3d a(Eigen::Vector3d::Zero());
   const Eigen::Vector3d b(Eigen::Vector3d::Constant(2));

   const Eigen::Vector3d result(fzero<Method::regulaFalsi>(a, b, testFunction, absTol));


   std::cout << result
             << "\n";
}

template <>
void test<Method::newton>() {
   const Eigen::Vector3d x(Eigen::Vector3d::Constant(2));

   std::cout << fzero<Method::newton>(x, testFunction, testFunctionDerivative)
             << "\n";
}


template <>
void test<Method::dampedNewton>() {
   const auto solveDFAt([] (ConstVectorRef x) {
      return ([x] (Vector b) -> Vector {
         b(0) /= 1 + x(0)*x(0);
         return b;
      });
   });

   const auto F([] (Vector x) -> Vector {
      x(0) = std::atan(x(0));
      return x;
   });

   const Vector x0(Vector::Constant(1, 3));
   std::cout << fzero<Method::dampedNewton>(x0, F, solveDFAt)
             << "\n";
}

int main() {
   test<Method::bisection>();
   test<Method::regulaFalsi>();
   test<Method::newton>();
   test<Method::dampedNewton>();

   return 0;
}

