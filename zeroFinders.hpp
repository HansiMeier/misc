#include <Eigen/Dense>
#include <Eigen/Core>
#include <cassert>
#include <stdexcept>
#include <type_traits>
#include <cmath>

namespace zeroFinders {


   /**************************************************************************************************
    *                                             TYPES                                              *
    **************************************************************************************************/

   template <typename Derived>
   using VectorBase = typename std::enable_if<
                                 Eigen::MatrixBase<Derived>::IsVectorAtCompileTime,
                                 Eigen::MatrixBase<Derived>
                               >::type;

   enum class Method {
      newton,
      dampedNewton,
      regulaFalsi,
      bisection
   };


   using Scalar = double;
   using Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
   using Index = Vector::Index;
   using ConstVectorRef = const Eigen::Ref<const Vector>&;

   template <Method toEnable, Method m>
   using VectorCheck = typename std::enable_if<toEnable == m, Vector>::type;


   /**************************************************************************************************
    *                                         DEFAULT VALUES                                         *
    **************************************************************************************************/

   Scalar defaultAbsTol(1e-8);
   Scalar defaultRelTol(1e-8);
   Index defaultMaxNumberOfSteps(1000);
   Scalar defaultMinimumDamping(1e-3);


   /**************************************************************************************************
    *                                           BISECTION                                            *
    **************************************************************************************************/

   // using this VectorCheck kludge because partial function specialization is illegal
   template <Method m, typename Function>
   VectorCheck<Method::bisection, m>
   fzero(Vector a,
         Vector b,
         const Function& F,
         Scalar absTol = defaultAbsTol,
         const Index maxNumberOfSteps = defaultMaxNumberOfSteps) {

      absTol *= absTol;

      assert(a.size() == b.size());
      const Index n(b.size());

      Vector mid(a.size());
      Vector functionValue(a.size());

      for (Index step(0); step < maxNumberOfSteps; ++step) {
         mid = (a + b)/2;
         functionValue = F(mid);
         for (Index i(0); i < n; ++i) {
            const Scalar scalarFunctionValue(functionValue[i]);
            if (scalarFunctionValue < 0) {
               a[i] = scalarFunctionValue;
            } else {
               b[i] = scalarFunctionValue;
            }
         }


         if ((b - a).squaredNorm() < absTol) {
            return mid;
         }
      }

      throw std::runtime_error("Bisection didn't converge!");
   }


   /**************************************************************************************************
    *                                          REGULA FALSI                                          *
    **************************************************************************************************/

   template <Method m, typename Function>
   VectorCheck<Method::regulaFalsi, m>
   fzero(Vector a,
         Vector b,
         const Function& F,
         Scalar absTol = defaultAbsTol,
         Scalar relTol = defaultRelTol,
         const Index maxNumberOfSteps = defaultMaxNumberOfSteps) {

      absTol *= absTol;
      relTol *= relTol;

      assert(a.size() == b.size());
      const Index n(b.size());
      Vector Fa(F(a));
      Vector Fb(F(b));
      Vector newPos;
      Vector newValue;
      Scalar andersonBjorkFactor;
      for (Index step(0); step < maxNumberOfSteps; ++step) {
         newPos = (a.cwiseProduct(Fb) - b.cwiseProduct(Fa)).cwiseQuotient(Fb - Fa);
         newValue = F(newPos);

         Scalar correctionNorm(0);
         Scalar correction;
         Scalar myNorm(0);

         for (Index i(0); i < n; ++i) {
            if ((Fa[i] >= 0) == (newValue[i] >= 0)) {
               correction = (a[i] - newPos[i]);
               Fa[i] = newValue[i];
               a[i] = newPos[i];
               myNorm += a[i]*a[i];
            } else {
               correction = b[i] - newPos[i];
               Fb[i] = newValue[i];
               b[i] = newPos[i];
               myNorm += b[i]*b[i];
               andersonBjorkFactor = (Fb[i] - newValue[i]) / Fb[i];
               if (andersonBjorkFactor < 0) {
                  andersonBjorkFactor = 0.5;
               }
               Fa[i] *= andersonBjorkFactor;
            }

            correction *= correction;
            correctionNorm += correction;
         }


         if (correctionNorm < absTol || correctionNorm < relTol * myNorm) {
            return newPos;
         }

      }

      throw std::runtime_error("Regula falsi didn't converge!");
   }


   /**************************************************************************************************
    *                                             NEWTON                                             *
    **************************************************************************************************/

   template <Method m, typename Function, typename Decomposition>
   VectorCheck<Method::newton, m>
   fzero(Vector x,
         const Function& F,
         const Decomposition& solveDFAt,
         Scalar absTol = defaultAbsTol,
         Scalar relTol = defaultRelTol,
         Scalar minimumDamping = defaultMinimumDamping,
         const Index maxNumberOfSteps = defaultMaxNumberOfSteps) {

      relTol *= relTol;
      absTol *= absTol;

      const Index n(x.size());
      Vector xNew(n);
      for (Index step(0);  step < maxNumberOfSteps; ++step) {
         xNew = x - solveDFAt(x)(F(x));

         const Scalar diffNormSquared((x - xNew).squaredNorm());
         if (diffNormSquared < absTol || diffNormSquared < relTol * xNew.squaredNorm()) {
            return xNew;
         }

         x = xNew;
      }

      throw std::runtime_error("Newton didn't converge!");
   }


   /**************************************************************************************************
    *                                         DAMPED NEWTON                                          *
    **************************************************************************************************/

   template <Method m, typename Function, typename Decomposition>
   VectorCheck<Method::dampedNewton, m>
   fzero(Vector x,
         const Function& F,
         const Decomposition& solveDFAt,
         Scalar absTol = defaultAbsTol,
         Scalar relTol = defaultRelTol,
         const Index maxNumberOfSteps = defaultMaxNumberOfSteps,
         const Scalar minimumDamping = defaultMinimumDamping) {

      relTol *= relTol;
      absTol *= absTol;

      const Index n(x.size());

      Vector newtonCorrection(n);
      Vector tentativeCorrection(n);
      Scalar damping(1);
      for (Index step(0); step < maxNumberOfSteps; ++step) {
         const auto solver(solveDFAt(x));
         newtonCorrection = solver(F(x));
         for (;; damping /= 2) {
            tentativeCorrection = solver(F(x - damping * newtonCorrection));
            Scalar factor(1 - damping/2);
            factor *= factor;

            if (tentativeCorrection.squaredNorm() < factor * newtonCorrection.squaredNorm()) {
               newtonCorrection *= damping;
               x -= newtonCorrection;
               damping *= 2;
               if (damping > 1) {
                  damping = 1;
               }
               break;
            } else if (damping < minimumDamping) {
               throw std::runtime_error("Damped newton reached minimum damping!");
            }

         }

         Scalar diffNormSquared(newtonCorrection.squaredNorm());
         if (diffNormSquared < absTol || diffNormSquared < relTol * x.squaredNorm()) {
            return x;
         }
      }

      throw std::runtime_error("Damped newton didn't converge!");
   }
}

