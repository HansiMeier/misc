#include <stdlib.h>
#include <immintrin.h>
#include <emmintrin.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <stdbool.h>


typedef void (*BinomialCalculator)(const unsigned long, const float*, const float*, float*);
#define ALIGNMENT 64

void
serialBinomial(
    unsigned long n,
    const float* x,
    const float* y,
    float* z
) {
    assert(n > 0);

    for (unsigned long i = 0; i < n; ++i) {
        z[i] = x[i] * x[i] + 2.0 * x[i] * y[i] + y[i] * y[i];
    }
}

void
avxBinomialWithIntrinsics(
    unsigned long n,
    const float* x,
    const float* y,
    float* z
) {


    for (; n != 0; z += 8, x += 8, y += 8, n -= 8) {

        __m256 xChunk = _mm256_load_ps(x);
        __m256 yChunk = _mm256_load_ps(y);

        __m256 zChunk = _mm256_mul_ps(xChunk, yChunk);
        zChunk = _mm256_add_ps(zChunk, zChunk);
        zChunk = _mm256_fmadd_ps(xChunk, xChunk, zChunk);
        zChunk = _mm256_fmadd_ps(yChunk, yChunk, zChunk);

        _mm256_store_ps(z, zChunk);
    }

}

void
sseBinomial(
    unsigned long n,
    const float* x,
    const float* y,
    float* z
) {
    assert(sizeof(*x) == 8 && sizeof(*y) == 8 && sizeof(*z) == 8);
    assert(n % 2 == 0);

    for (; n != 0; z += 4, x += 4, y += 4, n -= 4) {

        __m128 xChunk = _mm_load_ps(x);
        __m128 yChunk = _mm_load_ps(y);

        __m128 zChunk = _mm_mul_ps(xChunk, yChunk);
        zChunk = _mm_add_ps(zChunk, zChunk);
        zChunk = _mm_fmadd_ps(xChunk, xChunk, zChunk);
        zChunk = _mm_fmadd_ps(yChunk, yChunk, zChunk);

        _mm_store_ps(z, zChunk);
    }

}





void
initializeSpace(
    unsigned long n,
    float* x,
    float* y
) {
    *x = 0;
    *y = 0;
    long currentIndex = 0;
    for (; n != 0; ++x, ++y, ++currentIndex, --n) {
        *x = currentIndex+6;
        *y = currentIndex;
    }
}

void
_mm_freeGentle(
    void** memory
) {
    if (*memory != NULL) {
        _mm_free(*memory);
        *memory = NULL;
    }
}

int main(
) {
    BinomialCalculator candidatesToTest[] = {serialBinomial, avxBinomialWithIntrinsics, sseBinomial};
    const char* candidateDescriptions[] = {"serial version", "AVXed version", "SSEd version"};
    float sumTimings[sizeof(candidatesToTest) / sizeof(BinomialCalculator)] = {0}; // rest implicitly initialized with zero


    const unsigned long numberOfElements = 1UL << 25UL;
    const unsigned long numberOfRuns = 5;

    float* x = _mm_malloc(numberOfElements * sizeof(float), ALIGNMENT);
    float* y = _mm_malloc(numberOfElements * sizeof(float), ALIGNMENT);
    float* z = _mm_malloc(numberOfElements * sizeof(float), ALIGNMENT);
    if (x == NULL || y == NULL || z == NULL) {
        _mm_freeGentle((void**) &x);
        _mm_freeGentle((void**) &y);
        _mm_freeGentle((void**) &z);
        fputs("Failed to allocate memory!", stderr);
        return EXIT_FAILURE;
    }


    printf("Testing with vectors of %ld floats and the mean timing of %ld runs.\n", numberOfElements, numberOfRuns);

    const float relativeTolerance = 1e-5;
    double cpuTime;
    const unsigned long numberOfCandidates = sizeof(candidatesToTest) / sizeof(BinomialCalculator);

    for (unsigned long r = 0; r < numberOfRuns; ++r) {
        for (unsigned long c = 0; c < numberOfCandidates; ++c) {
            initializeSpace(numberOfElements, x, y);
            const clock_t clockBefore = clock();
            candidatesToTest[c](numberOfElements, x, y, z);
            const clock_t clockAfter = clock();
            cpuTime = ((double)(clockAfter - clockBefore)) / ((double) CLOCKS_PER_SEC);
            sumTimings[c] +=  cpuTime;

            // check that the computation was ok
            const float* readX = x;
            const float* readY = y;
            const float* checkZ = z;
            const float* xEnd = x + numberOfElements;
            for (; readX < xEnd; ++readX, ++readY, ++checkZ) {
                const float isResult = *checkZ;
                const float currentXValue = *readX;
                const float currentYValue = *readY;
                const float shouldBeResult = currentXValue * currentXValue + 2 * currentXValue * currentYValue + currentYValue * currentYValue;
                if (fabs(shouldBeResult - isResult) > relativeTolerance * shouldBeResult) {
                    fprintf(stderr, "Wrong result at index %ld for the %s: should be %f but is %f!\n", readX - x, candidateDescriptions[c], shouldBeResult, isResult);
                    _mm_freeGentle((void**) &x);
                    _mm_freeGentle((void**) &y);
                    _mm_freeGentle((void**) &z);
                    return EXIT_FAILURE;
                }
            }
        }
    }

    for (unsigned long c = 0; c < numberOfCandidates; ++c) {
        const float meanTiming = (float) sumTimings[c] / (float) numberOfRuns;
        printf("The %s took an average of %.9f seconds CPU time.\n", candidateDescriptions[c], meanTiming);
    }


    _mm_freeGentle((void**) &x);
    _mm_freeGentle((void**) &y);
    _mm_freeGentle((void**) &z);
    return EXIT_SUCCESS;
}


