#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <functional>
#include <iostream>
#include <cassert>
#include "dbg.hpp"
#include <vector>
#include "MathGl/Figure/figure.hpp"

//! Sparse Matrix type. Makes using this type easier.
typedef Eigen::SparseMatrix<double> SparseMatrix;

//! Used for filling the sparse matrix.


//! Vector type
typedef Eigen::VectorXd Vector;
typedef Eigen::MatrixXd Matrix;

typedef Vector::Index Index;

typedef const Eigen::Ref<const Vector>& ConstVectorRef;

//! Function signature of the function generating the Jacobi matrix for
//! the Newton iteration. The use case should be
//!
//! \code{.cpp}
//! // Input JacobiFunction JFun, Vector u
//! SparseMatrix DF;
//! JFun(DF, u);
//! \endcode
typedef std::function<void(SparseMatrix& DF, const Vector& u)> JacobiFunction;

//! Function signature of the function computing F(u) for
//! the Newton iteration. The use case should be
//! \code{.cpp}
//! // Input Function fun, Vector u
//! Vector Fu;
//! fun(Fu, u);
//! \endcode
typedef std::function<void(Vector& Fu, const Vector& u)> Function;



Vector
solveSymmetricTridiagonalSystem(
   Vector majorDiagonal,
   ConstVectorRef minorDiagonal,
   Vector rightHandSide
) {
   const Index n(majorDiagonal.size());
   assert(n == rightHandSide.size());
   assert(majorDiagonal.size() == minorDiagonal.size() + 1);

   #ifndef NDEBUG
   // Check that the matrix is positive definite
   Matrix systemMatrix(Matrix::Zero(n, n));
   systemMatrix.diagonal(0) = majorDiagonal;
   systemMatrix.diagonal(1) = minorDiagonal;
   systemMatrix.diagonal(-1) = minorDiagonal;

   assert(systemMatrix.ldlt().isPositive()); // only checks for semidefiniteness!
   assert(systemMatrix.determinant() > 0);

   const Vector untouchedRHS(rightHandSide);
   #endif


   // zero out upper minor diagonal
   const Vector eliminationFactors(minorDiagonal.cwiseQuotient(majorDiagonal.tail(n - 1)));
   rightHandSide.head(n - 1) -= eliminationFactors.cwiseProduct(rightHandSide.tail(n - 1));
   majorDiagonal.head(n - 1) -= eliminationFactors.cwiseProduct(minorDiagonal);

   // forward substitution
   rightHandSide(0) /= majorDiagonal(0);

   for (Index i(1); i < n - 1; ++i) {
      rightHandSide(i) -= rightHandSide(i - 1) * minorDiagonal(i - 1);
      rightHandSide(i) /= majorDiagonal(i);
   }


   // make sure error is small
   assert((systemMatrix * rightHandSide - untouchedRHS).norm() < 1e-10);

   return rightHandSide;
}


//! Solves the (non-linear) equation F(u)=0 using Newton's method.
//! \param[out] u at the end of the iteration, will contain the value of
//!                  the approximate solution to F(u)=0.
//! \param initialGuess Initial guess
//! \param F The function F
//! \param DF The jacobi of the function F
//! \param tolerance given tolerance. Iteration will stopp when
//!                        |u_n-u_{n-1}|<tolerance
void newtonSolve(Vector& u, const Vector& initialGuess, const Function& F,
                 const JacobiFunction& DF, const double tolerance) {

   const Index maxNumberOfRuns(1000);
   const Index dimension(u.size());


   Vector newtonCorrection(dimension);

   u = initialGuess;
   SparseMatrix J(dimension, dimension);
   Vector Fu(dimension);
   for (Index run(0); run < maxNumberOfRuns; ++run) {
      DF(J, u);
      F(Fu, u);
      newtonCorrection = Eigen::SparseLU<SparseMatrix>(J).solve(Fu);
      u -= newtonCorrection;
      if (newtonCorrection.norm() < tolerance * u.norm()) {
         return;
      }
   }

   throw std::runtime_error("Newton didn't converge!");

}

SparseMatrix
createBandedToeplitzMatrix(
   ConstVectorRef entriesOnDiagonals,
   const Index rows,
   const Index cols
) {
   const Index bandWidth(entriesOnDiagonals.size());

   assert(bandWidth % 2 == 1 && "Need to have an odd number of diagonals specified!");


   SparseMatrix toConstruct(rows, cols);
   toConstruct.reserve(Eigen::VectorXi::Constant(cols, bandWidth));

   // build up phase:
   // there are more nonzero entries on each column than on the column left of it
   const Index mainDiagonalIndex(bandWidth / 2);
   const Index buildUpRange(std::min(cols - 1, mainDiagonalIndex));
   Index c(0);
   for (; c <= buildUpRange; ++c) {
      const Index lastRow(std::min(rows - 1, mainDiagonalIndex + c));
      for (Index r(0), rBackwards(std::min(lastRow, bandWidth - 1)); r <= lastRow; ++r, --rBackwards) {
         toConstruct.insert(r, c) = entriesOnDiagonals(rBackwards);
      }
   }

   // main phase:
   // there are as many nonzero entries on each column as on the column left of it.
   const Index mainRange(std::min(cols - 1, c + rows - bandWidth - 1));
   Index offset(1);
   for (Index lastRow(std::min(bandWidth + offset - 1, rows - 1)); c <= mainRange; ++c, ++offset, ++lastRow) {
      for (Index r(offset), currentDiagonal(bandWidth - 1); r <= lastRow; ++r, --currentDiagonal) {
         toConstruct.insert(r, c) = entriesOnDiagonals(currentDiagonal);
      }
   }


   // tear down phase:
   // there are less nonzero entries on each column than on the column left of it
   for (; c < cols; ++c, ++offset) {
      for (Index r(offset), currentDiagonal(bandWidth - 1); r < rows; ++r, --currentDiagonal) {
         toConstruct.insert(r, c) = entriesOnDiagonals(currentDiagonal);
      }
   }


   toConstruct.makeCompressed();
   return toConstruct;
}

//! Create the Galerkin matrix A for 1D FEM
//! @param[out] A will be the Galerkin matrix (as in the exercise)
//! @param[in] N as in the exercise
//! @param[in] h the cell length
void createPoissonMatrix(SparseMatrix& A, int N, double h) {
   Vector entriesOnDiagonals(3);
   entriesOnDiagonals << -1.0 / h, 2.0 / h, -1.0 / h;
   A = createBandedToeplitzMatrix(entriesOnDiagonals, N, N);
   pvar(A);
   HALT;
}

//! Create the Jacobian matrix of \f$\sinh(\mu_i-f)\f$
//! @param[out] D will at the end contain the matrix D
//! @param[in] u the value of u
//! @param[in] N the number of nodes
//! @param[in] h the mesh width
//! @param[in] f the function f
void createDMatrix(SparseMatrix& D, const Vector u, int N, double h,
                   const std::function<double(double)>& f) {
   assert(N > 1);
   assert(D.rows() == D.cols() && D.rows() == N);
   assert(N == u.size());

   D.setZero();
   D.resize(N, N);
   D.reserve(Eigen::VectorXi::Constant(N, 1));


   D.insert(0, 0) = h / 2 * std::cosh(u(0) - f(0));
   for (Index i(1); i < N - 1; ++i) {
      D.insert(i, i) = h * std::cosh(u(i) - f(i * h));
   }
   D.insert(N - 1, N - 1) = h / 2 * std::cosh(u(N - 1) - f(1));
   pvar(D);
}

//! Creates the vector \f$\phi(u)\f$
//! \param u the current value of u
//! \param f the function f (given as a parameter)
//! \param N the number of basis functions
//! \param h the mesh width
//!
void createPhi(Vector& phi, const Vector& u, const std::function<double(double)>& f,
               int N, double h) {
   assert(phi.size() == N);
   for (Index i(0); i < N; ++i) {
      phi(i) = h * std::sinh(u(i) - f(i * h));
   }

   phi(0) /= 2;
   phi(N - 1) /= 2;
   pvar(phi);
}

//! Creates the Jacobi matrix
//! \param[out] DF will contain the jacobi matrix
//! \param[in] A the Poisson matrix
//! \param[in] u the approximation to u
//! \param[in] f the given function f
//! \param[in] N number of basis functions
//! \param[in] h mesh width
//!
void createJacobiMatrix(SparseMatrix& DF, const SparseMatrix& A,  const Vector& u,
                        const std::function<double(double)>& f,
                        int N, double h) {
   createDMatrix(DF, u, N, h, f);
   DF += A;
}


//! Compute an approximation to u
//!
//! @param[out] u will contain an approximation to u at end of invocation
//! @param[in] N the number of basis functions
//! @param[in] f the function f to use.
void computeU(Vector& u, const int N,
              const std::function<double(double)>& f) {
   const double h(1.0 / double(N));
   SparseMatrix A(N, N);
   createPoissonMatrix(A, N, h);

   const auto jacobianWrapper([&N, &h, &A, &f](SparseMatrix& DF, const Vector& u) -> void {
      createJacobiMatrix(DF, A, u, f, N, h);
   });

   const auto F([&A, &N, &f, &h] (Vector& Fu, const Vector& u) -> void {
      createPhi(Fu, u, f, N, h);
      Fu += A * u;
   });
   

   u.setZero();
   newtonSolve(u, u, F, jacobianWrapper, h);

}

int main() {
   const auto f([] (const double x) -> double {
      return x;
   });

   const char* lineStyles[] {"l+", "n*", "q#d"};
   mgl::Figure fig;
   Vector u;
   for (Index N(32), i(0); N <= 512; N *= 4, ++i) {
      u.resize(N);
      computeU(u, N, f);
      fig.plot(Vector::LinSpaced(N, 0, 1), u).style(lineStyles[i]).label("u_{" + std::to_string(N) + "}");
   }

   fig.xlabel("x");
   fig.ylabel("u(x)");
   fig.legend();
   fig.title("Nonlinear FEM");
   fig.save("nonLinearFEM");
   return 0;
}
