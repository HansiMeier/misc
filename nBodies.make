std:=-std=c++14
compileflags:=$(std) -Wall -O2
includes:=$(shell pkg-config --cflags eigen3) -I$(BOOSTINCLUDE) -I$(MATLABINCLUDE)
libdirs:=-L$(MATLABLIB)
linkerflags:=-Wl,-rpath,$(MATLABLIB) -lmx -lmat

ifdef NDEBUG
	compileflags+=-DNDEBUG
endif

n?=3
compileflags+=-DNUM_BODIES=$(n)

CXX?=g++

data: nBodies.mat

nBodies: nBodies.cpp
	$(CXX) $(compileflags) $(includes) $< $(libdirs) $(linkerflags) -o $@

nBodies.mat: nBodies
	./nBodies

