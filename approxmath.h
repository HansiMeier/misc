#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <math.h>
inline // Just to comply with the one definition rule
double
approxSin(
    double x
) {
    double sign(1.0);
    if (x >= M_PI) {
        sign *= -1.0;
        x = 2*M_PI - x;
    }

    if (x >= M_PI_2) {
        x = M_PI - x;
    }

    // The best L^2 approximant on [0, M_PI/2] of all polynomials of degree two according to Wolfram's NMinimize
    // abs error ~ 1e-3
    return sign * (-0.024324946963099683 + x * (1.195745064265028 + x * -0.3382400105162437));
}

inline // Just to comply with the one definition rule
double
approxCos(
    double x
) {
    double sign(1.0);
    if (x >= M_PI) {
        x = 2*M_PI - x;
    }

    if (x >= M_PI_2) {
        sign *= -1.0f;
        x = M_PI - x;
    }

    // The best L^2 approximant on [0, M_PI/2] of all polynomials of degree two according to Wolfram's NMinimize
    // abs error ~ 1e-3
    return sign * (1.019373224388285 + x * (-0.1331327050489433 + x * -0.33824003354674276));
}




#ifdef __cplusplus
}
#endif

